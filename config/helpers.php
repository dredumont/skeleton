<?php

if (!function_exists('image_category')) {
    function image_category($path, $category = null)
    {
        if (!$category) {
            return $path;
        }

        $basename = basename($path);

        return str_replace($basename, $category . '/' . $basename, $path);
    }
}

if (!function_exists('page_route')) {
    function page_route($ident, $locale = '')
    {
        $page = \App\Models\Page::where('active', true)
            ->where('ident', $ident)
            ->where('locale', $locale)
            ->first();

        if ($page) {
            return Request::root() . '/' . $page->url;
        }
    }
}

if (!function_exists('switch_lang')) {
    function switch_lang($locale)
    {
        if (config('app.currentPage')) {
            $page = \App\Models\Page::where('active', true)
                ->where('ident', config('app.currentPage')->ident)
                ->where('locale', $locale)
                ->first();

            if ($page) {
                return Request::root() . '/' . $page->url;
            }
        }

        return LaravelLocalization::getLocalizedURL($locale);
    }
}

if (!function_exists('img_url')) {
    function img_url($image, $params = [])
    {
        return url() . GlideImage::load($image, $params);
    }
}

if (!function_exists('restful_controller')) {
    function restful_controller($controller)
    {
        $className = studly_case($controller);

        Route::get($controller, [
            'as' => 'admin.' . $controller,
            'uses' => $className . 'Controller@index'
        ]);
        Route::get($controller . '/create', [
            'as' => 'admin.' . $controller . '.create',
            'uses' => $className . 'Controller@create'
        ]);
        Route::post($controller, [
            'as' => 'admin.' . $controller . '.store',
            'uses' => $className . 'Controller@store'
        ]);
        Route::get($controller . '/{' . $controller . '}/edit', [
            'as' => 'admin.' . $controller . '.edit',
            'uses' => $className . 'Controller@edit'
        ]);
        Route::put($controller . '/{' . $controller . '}', [
            'as' => 'admin.' . $controller . '.update',
            'uses' => $className . 'Controller@update'
        ]);
        Route::delete($controller . '/{' . $controller . '}', [
            'as' => 'admin.' . $controller . '.destroy',
            'uses' => $className . 'Controller@destroy'
        ]);
    }
}