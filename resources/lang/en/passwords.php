<?php

return array (
  'password' => 'Passwords must be at least six characters and match the confirmation.',
  'user' => 'We can\'t find a user with that e-mail address.',
  'token' => 'This password reset token is invalid.',
  'sent' => 'We have e-mailed your password reset link.',
  'reset' => 'Your password has been reset.',
  'Reset your password' => 'Reset your password',
  'reset_email_2' => 'We got a request to reset your password.',
  'reset_email_1' => 'Hi :firstname,',
  'reset_email_3' => 'If you ignore this message, your password won\'t be changed.',
  'reset_email_4' => 'If you didn\'t request a password reset, please',
  'reset_email_5' => 'contact us.',
  'Enter your email to receive a reset link' => 'Enter your email to receive a reset link',
  'Enter your email and your new password' => 'Enter your email and your password',
);
