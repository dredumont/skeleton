<?php

return array (
  'password' => 'Les mots de passe doivent comporter au moins six caractères et correspondre à la confirmation.',
  'user' => 'Nous ne pouvons pas trouver un utilisateur avec cette adresse email.',
  'token' => 'Ce jeton de réinitialisation de mot de passe n\'est pas valide.',
  'sent' => 'Nous vous avons envoyé par courriel votre lien de réinitialisation de mot de passe.',
  'reset' => 'Votre mot de passe a été réinitialisé.',
  'Reset your password' => 'Réinitialiser votre mot de passe',
  'Enter your email to receive a reset link' => 'Entrez votre courriel pour recevoir un lien de réinitialisation',
  'reset_email_1' => 'Bonjour :firstname,',
  'reset_email_2' => 'Nous avons reçu une demande pour réinitialiser votre mot de passe.',
  'reset_email_3' => 'Si vous ignorez ce message, votre mot de passe ne sera pas modifié.',
  'reset_email_4' => 'Si vous n\'avez pas fait cette demande, veuillez nous',
  'reset_email_5' => 'contacter.',
  'Enter your email and your new password' => 'Entrez votre courriel et votre mot de passe',
);
