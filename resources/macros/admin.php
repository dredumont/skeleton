<?php

\Form::macro('adminImage', function ($input, $controller = '', $item = null, $name = null, $isMultiple = false, $multipleKey = null, $locale = null) {
    if ($item) {
        if ($locale) {
            $value = old(str_replace('_', '.', $input)) ?: ($item->hasTranslation($locale) ? $item->{str_replace($locale . '_', '', $input) . ':' . $locale} : $item->{$input});
        } else {
            $value = old($input) ?: $item->{$input};
        }
    } else {
        if ($locale) {
            $value = old(str_replace('_', '.', $input));
        } else {
            $value = old($input);
        }
    }

    if (is_null($name)) {
        $name = $input;
    }

    if ($isMultiple) {
        if ($item) {
            $value = old($name)[$multipleKey] ?: json_decode($item->{$name})[$multipleKey];
        } else {
            $value = old($name)[$multipleKey];
        }

        $name.= '[]';
    }

    return '
        <div class="img-group">
            <div class="pull-left">
                <input type="hidden" id="' . $input . '" name="' . $name . '" class="img-hidden" value="' . $value . '">
                <a href="' . ($value ? img_url($value, ['fit' => 'max', 'w' => 750]) : '') . '" class="btn-show-image">
                    <img src="' . ($value ? img_url($value, ['fit' => 'crop', 'h' => 150, 'w' => 150]) : '') . '" class="img-admin img-thumbnail ' . $input . '">
                </a>
            </div>
            <div class="pull-left img-admin-actions">
                <a href="' . url('admin/elfinder/popup') . '" class="btn btn-primary btn-add-image" data-inputid="' . $input . '" data-category="' . $controller . '">
                    <i class="fa fa-plus"></i>
                </a>
                <a href="#" class="btn btn-danger btn-delete-image">
                    <i class="fa fa-times"></i>
                </a>
            </div>
            <div class="clearfix"></div>
        </div>
    ';
});

\Form::macro('adminFile', function($input, $item = null, $name = null, $isMultiple = false, $multipleKey = null, $locale = null) {
    if ($item) {
        if ($locale) {
            $value = old(str_replace('_', '.', $input)) ?: ($item->hasTranslation($locale) ? $item->{str_replace($locale . '_', '', $input) . ':' . $locale} : $item->{$input});
        } else {
            $value = old($input) ?: $item->{$input};
        }
    } else {
        if ($locale) {
            $value = old(str_replace('_', '.', $input));
        } else {
            $value = old($input);
        }
    }

    if (is_null($name)) {
        $name = $input;
    }

    if ($isMultiple) {
        if ($item) {
            $value = old($name)[$multipleKey] ?: json_decode($item->{$name})[$multipleKey];
        } else {
            $value = old($name)[$multipleKey];
        }

        $name.= '[]';
    }

    return '
        <div class="file-group">
            <div class="file-admin-actions" style="margin-bottom: 5px;">
                <a href="' . url('admin/elfinder/popup') . '" class="btn btn-primary btn-add-file" data-inputid="' . $input . '">
                    <i class="fa fa-plus"></i>
                </a>
                <a href="#" class="btn btn-danger btn-delete-file">
                    <i class="fa fa-times"></i>
                </a>
            </div>
            <div>
                <input type="hidden" id="' . $input . '" name="' . $name . '" class="file-hidden" value="' . $value . '">
                <a target="_blank" class="' . $input . '" href="' . $value . '">
                    <input type="text" class="form-control" value="' . $value . '" style="cursor: pointer;" readonly>
                </a>
            </div>
        </div>
    ';
});

\Form::macro('one', function ($model, $name, $list = array(), $selected = null, $options = array()) {
//    $selected = $this->getValueAttribute($name, $selected);
//
//    $options['id'] = $this->getIdAttribute($name, $options);
//
//    if ( ! isset($options['name'])) $options['name'] = $name;
//
//    $html = array();
//
//    foreach ($list as $value => $display)
//    {
//        $html[] = $this->getSelectOption($display, $value, $selected);
//    }
//
//    $options = $this->html->attributes($options);
//
//    $list = implode('', $html);
//
//    return "<select{$options}>{$list}</select>";
});

\Form::macro('many', function ($model, $name, $list = array(), $selected = null, $options = array()) {
//    $selected = $this->getValueAttribute($name, $selected);
//
//    $options['id'] = $this->getIdAttribute($name, $options);
//
//    if ( ! isset($options['name'])) $options['name'] = $name;
//
//    $html = array();
//
//    foreach ($list as $value => $display)
//    {
//        $html[] = $this->getSelectOption($display, $value, $selected);
//    }
//
//    $options = $this->html->attributes($options);
//
//    $list = implode('', $html);
//
//    return "<select{$options} multiple>{$list}</select>";
});
