{{--Default--}}
@extends('frontend.layout')

@section('content')

    <section>
        <h1>{{ $page->title }}</h1>
        <div>
            {!! $page->content !!}
        </div>
    </section>

@endsection