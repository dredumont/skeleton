<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <base href="{{ url() }}/" data-current="{{ \Request::url() }}">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{ $pageTitle . ' | ' . trans('base.site_title') }}</title>
        <meta name="title" content="{{ $pageTitle . ' | ' . trans('base.site_title') }}">
        <meta name="description" content="{{ $pageDescription or trans('base.site_description') }}">
        @yield('metas')

        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('frontend/css/styles.css') }}">
        @yield('styles')
    </head>
    <body class="page_{{ $controller or "" }}{{ $action or "" }}">

        @include('frontend.partials.navigation')

        <div class="container main">
            @include('frontend.partials.breadcrumb')

            @yield('content')
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <script src="{{ asset('frontend/js/scripts.js') }}"></script>
        @yield('scripts')
    </body>
</html>
