<ol class="breadcrumb">
    <li><a href="{{ route('site.home') }}">{{ trans('home.page_title') }}</a></li>
    @if(isset($breadcrumb))
        @foreach($breadcrumb['list'] as $item)
            <li><a href="{{ $item['url'] }}">{{ $item['title'] }}</a></li>
        @endforeach
        <li class="active">{{ $breadcrumb['current']['title'] }}</li>
    @endif
</ol>