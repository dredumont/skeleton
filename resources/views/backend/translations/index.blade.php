@extends('backend.layout')

@section('styles')

    <style>
        a.status-1 { font-weight: bold; }
    </style>

@endsection

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>{{ $title }}</h1>
    </section>

    <!-- Main content -->
    <section class="content" data-content="{{ explode('@', Route::currentRouteAction())[1] }}">
        <div class="row">
            <div class="col-xs-12 col-sm-9 col-md-8 col-lg-6">
                <div class="box">
                    <div class="box-body">
                        <div class="alert alert-success success-import" style="display:none;">
                            <p>{{ trans('backend.Done importing, processed') }} <strong class="counter">N</strong> items! {{ trans('backend.Reload this page to refresh the groups!') }}</p>
                        </div>
                        <div class="alert alert-success success-find" style="display:none;">
                            <p>{{ trans('backend.Done searching for translations, found') }} <strong class="counter">N</strong> items!</p>
                        </div>
                        <div class="alert alert-success success-publish" style="display:none;">
                            <p>{{ trans('backend.Done publishing the translations for group') }} "{{ $group }}"!</p>
                        </div>

                        @if(Session::has('successPublish'))
                            <div class="alert alert-info">
                                {{ Session::get('successPublish') }}
                            </div>
                        @endif

                        <form class="form-inline form-import" method="POST" action="{{ route('admin.translations.postImport') }}" data-remote="true" role="form">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <select name="replace" class="form-control">
                                <option value="0">{{ trans('backend.Append new translations') }}</option>
                                <option value="1">{{ trans('backend.Replace existing translations') }}</option>
                            </select>
                            <button type="submit" class="btn btn-success"  data-disable-with="{{ trans('backend.Loading') }}...">{{ trans('backend.Import groups') }}</button>
                        </form>
                        <br/>
                        <form class="form-inline form-find" method="POST" action="{{ route('admin.translations.postFind') }}" data-remote="true" role="form" data-confirm="{{ trans('backend.Are you sure you want to scan you app folder? All found translation keys will be added to the database.') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <select name="path" class="form-control">
                                <option value="">{{ trans('backend.Everywhere') }}</option>
                                <option value="app">{{ trans('backend.Application') }}</option>
                                <option value="resources">{{ trans('backend.Resources') }}</option>
                                <option value="vendor">{{ trans('backend.Vendor (Heavy task)') }}</option>
                            </select>
                            <button type="submit" class="btn btn-info" data-disable-with="{{ trans('backend.Searching') }}..." >{{ trans('backend.Find translations in files') }}</button>
                        </form>
                        <br/>
                        <form role="form">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <select name="group" id="group" class="form-control group-select">
                                    @foreach($groups as $key => $value)
                                        <option value="{{ $key }}"{{ $key == $group ? ' selected': '' }}>{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /.content -->

@endsection

@section('scripts')

    <script>
        var moduleUrl = '{{ route('admin.translations') }}';
        var csrfToken = '{{ csrf_token() }}';
    </script>

    <script src="{{ asset('backend/js/admin.translations.js') }}"></script>

@endsection