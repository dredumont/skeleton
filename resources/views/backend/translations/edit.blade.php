@extends('backend.layout')

@section('styles')

    <style>
        a.status-1 { font-weight: bold; }
    </style>

@endsection

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>{{ $title }}</h1>
    </section>

    <!-- Main content -->
    <section class="content" data-content="{{ explode('@', Route::currentRouteAction())[1] }}">
        <div class="row">
            <div class="col-xs-12 col-sm-9 col-md-8 col-lg-6">
                <div class="box">
                    <div class="box-body">
                        <div class="alert alert-success success-import" style="display:none;">
                            <p>{{ trans('backend.Done importing, processed') }} <strong class="counter">N</strong> items! {{ trans('backend.Reload this page to refresh the groups!') }}</p>
                        </div>
                        <div class="alert alert-success success-find" style="display:none;">
                            <p>{{ trans('backend.Done searching for translations, found') }} <strong class="counter">N</strong> items!</p>
                        </div>
                        <div class="alert alert-success success-publish" style="display:none;">
                            <p>{{ trans('backend.Done publishing the translations for group') }} "{{ $group }}"!</p>
                        </div>

                        @if(Session::has('successPublish'))
                            <div class="alert alert-info">
                                {{ Session::get('successPublish') }}
                            </div>
                        @endif

                        <form class="form-inline form-publish" method="POST" action="{{ route('admin.translations.postPublish', ['group' => $group]) }}" data-remote="true" role="form" data-confirm="Are you sure you want to publish the translations group '<?= $group ?>? This will overwrite existing language files.">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <button type="submit" class="btn btn-info" data-disable-with="{{ trans('backend.Publishing') }}..." >{{ trans('backend.Publish translations') }}</button>
                        </form>
                        <br/>
                        <form role="form">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <select name="group" id="group" class="form-control group-select">
                                    @foreach($groups as $key => $value)
                                        <option value="{{ $key }}"{{ $key == $group ? ' selected': '' }}>{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </form>
                        <form action="{{ route('admin.translations.postAdd', [$group]) }}" method="POST" role="form">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <textarea class="form-control" rows="3" name="keys" placeholder="{{ trans('backend.Add 1 key per line, without the group prefix') }}"></textarea>
                            <br/>
                            <input type="submit" value="{{ trans('backend.Add keys') }}" class="btn btn-primary">
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <hr/>
                        <h4>{{ trans('backend.Total') }}: {{ $numTranslations }} - {{ trans('backend.Changed') }}: {{ $numChanged }}</h4>
                        <table id="table" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th class="text-center">
                                        <input type="checkbox" class="simple select-all">
                                    </th>
                                    <th>{{ trans('backend.Key') }}</th>
                                    @foreach($locales as $locale)
                                        <th>{{ $locale }}</th>
                                    @endforeach
                                    @if($deleteEnabled)
                                        <th>&nbsp;</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($translations as $key => $translation)
                                    <tr id="{{ $key }}">
                                        <td class="text-center">
                                            <input type="checkbox" class="simple select-single">
                                        </td>
                                        <td>{{ $key }}</td>
                                        @foreach($locales as $locale)
                                            <?php $t = isset($translation[$locale]) ? $translation[$locale] : null?>

                                            <td>
                                                <a href="#edit" class="editable status-{{ $t ? $t->status : 0 }} locale-{{ $locale }}" data-locale="{{ $locale }}" data-name="{{ $locale . "|" . $key }}" id="username" data-type="textarea" data-pk="{{ $t ? $t->id : 0 }}" data-url="{{ $editUrl }}" data-title="{{ trans('backend.Enter translation') }}">{{ $t ? htmlentities($t->value, ENT_QUOTES, 'UTF-8', false) : '' }}</a>
                                            </td>
                                        @endforeach
                                        @if($deleteEnabled)
                                            <td>
                                                <a href="{{ route('admin.translations.postDelete', [$group, $key]) }}" class="delete btn btn-danger btn-xs" data-token="{{ csrf_token() }}" data-confirm-text="{{ trans('backend.Are you sure ?') }}"><i class="fa fa-trash"></i> {{ trans('backend.Delete') }}</a>
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th class="text-center">
                                        <a href="javascript:void(0);" class="delete-selected btn btn-danger btn-xs"><i class="fa fa-trash"></i> {{ trans('backend.Delete') }}</a>
                                    </th>
                                    <th>{{ trans('backend.Key') }}</th>
                                    @foreach($locales as $locale)
                                        <th>{{ $locale }}</th>
                                    @endforeach
                                    @if($deleteEnabled)
                                        <th>&nbsp;</th>
                                    @endif
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /.content -->

@endsection

@section('scripts')

    <script>
        var moduleUrl = '{{ route('admin.translations') }}';
        var csrfToken = '{{ csrf_token() }}';
    </script>

    <script src="{{ asset('backend/js/admin.translations.js') }}"></script>

@endsection