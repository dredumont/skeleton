<header class="main-header">
    <!-- Logo -->
    <a href="{{ route('admin') }}" class="logo">
        @if(file_exists(public_path('backend/img/logo.png')))
            <img src="{{ asset('backend/img/logo.png') }}" title="{{ trans('base.site_title') }}" class="icon">
        @else
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>{{ substr(trans('base.site_title'), 0, 1) }}</b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>{{ trans('base.site_title') }}</b></span>
        @endif
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                @if(count(config('laravellocalization.supportedLocales')) > 1)
                    @foreach(config('laravellocalization.supportedLocales') as $locale => $param)
                        <li class="{{ config('app.locale') === $locale ? 'active' : '' }}">
                            <a href="{{ switch_lang($locale) }}">{{ strtoupper($locale) }}</a>
                        </li>
                    @endforeach
                @endif
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{ Auth::user()->avatar ? img_url(Auth::user()->avatar, ['fit' => 'crop', 'h' => 150, 'w' => 150]) : asset('backend/img/avatar.jpg') }}" class="user-image" alt="User Image">
                        <span class="hidden-xs">{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="{{ Auth::user()->avatar ? img_url(Auth::user()->avatar, ['fit' => 'crop', 'h' => 150, 'w' => 150]) : asset('backend/img/avatar.jpg') }}" class="img-circle" alt="User Image">

                            <p>
                                {{ Auth::user()->firstname }} {{ Auth::user()->lastname }}
                                <small>{{ trans('backend.Member since') }} {{ date('M' . (strlen(date('M') > 3) ? '.' : '') . ' Y', strtotime(Auth::user()->created_at)) }}</small>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            @role('super.admin')
                                <div class="pull-left">
                                    <a href="{{ route('admin.users.edit', Auth::user()->id) }}" class="btn btn-default btn-flat">{{ trans('backend.Profile') }}</a>
                                </div>
                                <div class="pull-right">
                            @endrole
                                <a href="{{ route('admin.logout') }}" class="btn btn-default btn-flat">{{ trans('backend.Sign out') }}</a>
                            @role('super.admin')
                                </div>
                            @endrole
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>