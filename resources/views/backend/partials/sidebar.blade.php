<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
        <div class="pull-left image">
            <img src="{{ Auth::user()->avatar ? img_url(Auth::user()->avatar, ['fit' => 'crop', 'h' => 150, 'w' => 150]) : asset('backend/img/avatar.jpg') }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
            <p>{{ trans('backend.Hello') }}, {{ Auth::user()->firstname }}</p>
        </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
        <li class="header">MENU</li>
        @foreach (config('admin.menu') as $route => $menu)
            @if (Auth::user()->is('super.admin') or (Auth::user()->is('admin') and $menu['super_admin_only'] == false))
                @if (isset($menu['submenu']))
                    <li class="treeview {{ in_array('admin.' . $controller, array_keys($menu['submenu'])) ? 'active' : '' }}">
                        <a href="#">
                            <i class="fa {{ $menu['icon'] }}"></i>
                            <span>{{ $menu['title'] }}</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            @foreach ($menu['submenu'] as $subMenuRoute => $subMenu)
                                @if (Auth::user()->is('super.admin') or (Auth::user()->is('admin') and $subMenu['super_admin_only'] == false))
                                    @if(Route::getRoutes()->hasNamedRoute($subMenuRoute))
                                        <li class="{{ 'admin.' . $controller === $subMenuRoute ? 'active' : '' }}">
                                            <a href="{{ route($subMenuRoute) }}">
                                                <i class="fa fa-angle-double-right"></i> {{ $subMenu['title'] }}
                                            </a>
                                        </li>
                                    @else
                                        <li class="{{ 'admin.' . $controller === $subMenuRoute ? 'active' : '' }}">
                                            <a href="javascript:void(0);">
                                                <i class="fa fa-exclamation-circle text-red"></i> {{ $subMenu['title'] }}
                                            </a>
                                        </li>
                                    @endif
                                @endif
                            @endforeach
                        </ul>
                    </li>
                @else
                    <li class="{{ 'admin.' . $controller === $route ? 'active' : '' }}">
                        @if(Route::getRoutes()->hasNamedRoute($route))
                            <a href="{{ route($route) }}">
                                <i class="fa {{ $menu['icon'] }}"></i> <span>{{ $menu['title'] }}</span>
                            </a>
                        @else
                            <a href="javascript:void(0);">
                                <i class="fa fa-exclamation-circle text-red"></i> <span>{{ $menu['title'] }}</span>
                            </a>
                        @endif
                    </li>
                @endif
            @endif
        @endforeach
    </ul>
</section>
<!-- /.sidebar -->