<footer class="main-footer">
    @if(file_exists(base_path('version.txt')))
    <div class="pull-right hidden-xs">
        <b>Version</b> {{ file_get_contents(base_path('version.txt')) }}
    </div>
    @endif
    <strong>Copyright &copy; {{ date('Y') }} <a target="_blank" href="http://www.reptiletech.com">Reptiletech</a></strong>. {{ trans('backend.All rights reserved.') }}
</footer>