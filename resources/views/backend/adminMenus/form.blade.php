@extends('backend.layout')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>{{ $title }}</h1>
    </section>

    <!-- Main content -->
    <section class="content" data-content="{{ explode('@', Route::currentRouteAction())[1] }}">
        <div class="row">
            <div class="col-xs-12 col-sm-9 col-md-8 col-lg-6">
                <div class="box">
                    <div class="box-body">
                        {!! Form::open(['route' => 'admin.' . $controller . '.store', 'autocomplete' => 'off']) !!}
                            <input type="hidden" name="menu_items_delete" value="">
                            <div class="form-group">
                                <button type="button" class="btn btn-sm btn-info btn-add-item" data-ajax-action="{{ route('admin.adminMenus.ajax') }}">{{ trans('backend.Add an item') }}</button>
                            </div>
                            <div class="form-group">
                                <div class="panel-group" id="menu-items">
                                    <input type="hidden" name="menu_items_tree" value="">
                                    <ul class="menu-items">
                                        @foreach($itemList as $menuItem)
                                            @include('backend.adminMenus.partials.menuItem')
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::submit(trans('backend.Submit'), ['class' => 'btn btn-primary']) !!}
                                <div class="pull-right text-right">
                                    <a href="{{ route('admin.' . $controller) }}" class="cancel btn btn-warning">{{ trans('backend.Cancel') }}</a>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /.content -->

@endsection

@section('scripts')

    <script src="{{ asset('backend/js/plugins/nestedSortable/jquery.nestedSortable.js') }}"></script>
    <script src="{{ asset('backend/js/admin.adminMenus.js') }}" type="text/javascript"></script>

@endsection