<li id="menu-item-{{ $menuItem->id }}">
    <div class="panel panel-info">
        <div class="panel-heading">
            <h4 class="panel-title">
                <i class="handle fa fa-arrows"></i>
                <a data-toggle="collapse" data-parent="#menu-items" href="#menu-item-info-{{ $menuItem->id }}">{{ $menuItem->title }}</a>
                <a class="btn-delete-item" href="javascript:void(0);" data-ajax-action="{{ route('admin.adminMenus.ajax') }}" data-menu-item-id="{{ $menuItem->id }}"><i class="pull-right fa fa-times"></i></a>
            </h4>
        </div>
        <div id="menu-item-info-{{ $menuItem->id }}" class="panel-collapse collapse">
            <div class="panel-body">
                <div class="form-group">
                    {!! Form::label('title-' . $menuItem->id, trans('backend.Title')) !!}
                    {!! Form::text('title-' . $menuItem->id, $menuItem->title, ['class' => 'form-control', 'name' => 'menu_items[' . $menuItem->id . '][title]']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('route-' . $menuItem->id, trans('backend.Route')) !!}
                    {!! Form::text('route-' . $menuItem->id, $menuItem->route, ['class' => 'form-control', 'name' => 'menu_items[' . $menuItem->id . '][route]']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('icon-' . $menuItem->id, trans('backend.Icon')) !!}
                    {!! Form::text('icon-' . $menuItem->id, $menuItem->icon, ['class' => 'form-control', 'name' => 'menu_items[' . $menuItem->id . '][icon]']) !!}
                </div>
                <div class="form-group">
                    <input type="hidden" name="menu_items[{{ $menuItem->id }}][super_admin_only]" value="0">
                    {!! Form::checkbox('menu_items[' . $menuItem->id . '][super_admin_only]', 1, $menuItem->super_admin_only, ['class' => 'simple', 'id' => 'super-admin-only-' . $menuItem->id]) !!}
                    {!! Form::label('super-admin-only-' . $menuItem->id, trans('backend.Super Admin Only')) !!}
                </div>
                <div class="form-group">
                    <input type="hidden" name="menu_items[{{ $menuItem->id }}][active]" value="0">
                    {!! Form::checkbox('menu_items[' . $menuItem->id . '][active]', 1, $menuItem->active, ['class' => 'simple', 'id' => 'active-' . $menuItem->id]) !!}
                    {!! Form::label('active-' . $menuItem->id, trans('backend.Active')) !!}
                </div>
            </div>
        </div>
    </div>
    @if(is_numeric($menuItem->id) && $menuItem->children)
        <ul>
            @foreach($menuItem->children()->orderBy('position', 'ASC')->get() as $menuItem)
                @include('backend.adminMenus.partials.menuItem')
            @endforeach
        </ul>
    @endif
</li>
