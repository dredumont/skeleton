@extends('backend.layout')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>{{ $title }}</h1>
    </section>

    <!-- Main content -->
    <section class="content" data-content="{{ explode('@', Route::currentRouteAction())[1] }}">
        <div class="row">
            <div class="col-xs-12 col-sm-9 col-md-8 col-lg-6">
                <div class="box">
                    <div class="box-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if($item->id)
                        {!! Form::open(['route' => ['admin.' . $controller . '.update', $item->id], 'method' => 'PUT', 'autocomplete' => 'off']) !!}
                        @else
                        {!! Form::open(['route' => 'admin.' . $controller . '.store', 'autocomplete' => 'off']) !!}
                        @endif
                            <div class="form-group">
                                {!! Form::label('title', trans('backend.Title')) !!}
                                {!! Form::text('title', $item->title, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('slug', trans('backend.Slug')) !!}
                                {!! Form::text('slug', $item->slug, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('url', trans('backend.Url')) !!}
                                {!! Form::text('url', $item->url, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('parent_id', trans('backend.Parent')) !!}
                                <select class="form-control select2" name="parent_id" id="parent_id">
                                    <option value="">{{ trans('backend.None') }}</option>
                                    @foreach(\App\Models\Page::where('locale', '')->orderBy('title')->get() as $page)
                                        <option data-url="{{ $page->url }}" data-locale="" value="{{ $page->id }}" {{ old('parent_id', $item->parent_id) == $page->id ? 'selected' : '' }}>{{ $page->title }}</option>
                                    @endforeach
                                    @foreach(config('laravellocalization.supportedLocales') as $locale => $param)
                                        <optgroup label="{{ $param['name'] }}"></optgroup>
                                        @foreach(\App\Models\Page::where('locale', $locale)->orderBy('title')->get() as $page)
                                            <option data-url="{{ $page->url }}" data-locale="{{ $page->locale }}" value="{{ $page->id }}" {{ old('parent_id', $item->parent_id) == $page->id ? 'selected' : '' }}>{{ $page->title }}</option>
                                        @endforeach
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                {!! Form::label('locale', trans('backend.Locale')) !!}
                                {!! Form::hidden('locale', null, ['id' => 'hidden_locale']) !!}
                                {!! Form::select('locale', $localeList, old('locale', $item->locale), ['class' => 'form-control select2']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('ident',trans('backend.Ident (For multilanguage pages, enter the same for all languages)')) !!}
                                <?php $identList = DB::table('pages')->select('ident')->groupBy('ident')->lists('ident'); ?>
                                <select id="group_ident" class="form-control select2">
                                    <option value="">{{trans('backend.Custom') }}</option>
                                    @foreach($identList as $ident)
                                        <option value="{{ $ident }}" {{ $item->ident === $ident ? 'selected' : '' }}>{{ $ident }}</option>
                                    @endforeach
                                </select>
                                {!! Form::text('ident', $item->ident, ['class' => 'form-control', (in_array($item->ident, $identList) ? 'readonly' : '') => (in_array($item->ident, $identList) ? 'readonly' : '')]) !!}
                            </div>
                            <hr>
                            <div class="form-group">
                                {!! Form::label('template', trans('backend.Template')) !!}
                                {!! Form::select('template', $templateList, old('template', $item->template), ['class' => 'form-control select2']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('publish_date', trans('backend.Publish Date')) !!}
                                {!! Form::text('publish_date', $item->publish_date, ['class' => 'form-control datetimepicker']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('expiry_date', trans('backend.Expiry Date')) !!}
                                {!! Form::text('expiry_date', $item->expiry_date, ['class' => 'form-control datetimepicker']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('content', trans('backend.Content')) !!}
                                {!! Form::textarea('content', $item->content, ['class' => 'form-control ckeditor']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('image', trans('backend.Image')) !!}
                                {!! Form::adminImage('image', $controller, $item) !!}
                            </div>
                            <hr>
                            <div class="form-group">
                                {!! Form::label('meta_title', trans('backend.Meta Title')) !!}
                                {!! Form::text('meta_title', $item->meta_title, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('meta_description', trans('backend.Meta Description')) !!}
                                {!! Form::textarea('meta_description', $item->meta_description, ['class' => 'form-control', 'rows' => 3]) !!}
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="active" value="0">
                                {!! Form::checkbox('active', 1, $item->active, ['class' => 'simple', 'id' => 'active']) !!}
                                {!! Form::label('active', trans('backend.Active')) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::submit(trans('backend.Submit'), ['class' => 'btn btn-primary']) !!}
                                <div class="pull-right text-right">
                                    <a href="{{ route('admin.' . $controller) }}" class="cancel btn btn-warning">{{ trans('backend.Cancel') }}</a>
                                    @if($item->id)
                                        <a href="{{ route('admin.' . $controller . '.destroy', ['item' => $item->id]) }}" class="delete btn btn-danger" data-token="{{ csrf_token() }}" data-confirm-text="{{ trans('backend.Are you sure ?') }}">{{ trans('backend.Delete') }}</a>
                                    @endif
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /.content -->

@endsection

@section('scripts')

    <script src="{{ asset('backend/js/admin.pages.js') }}" type="text/javascript"></script>

@endsection