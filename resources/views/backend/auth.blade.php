<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <base href="{{ url() }}" data-current="{{ \Request::url() }}">
    <title>{{ $title }} - {{ trans('base.site_title') }}</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta content="noindex, nofollow" name="robots">
    <meta content="{{ csrf_token() }}" name="csrf-token">

    <!-- Admin LTE Plugins -->
    <link href="{{ asset('packages/adminlte/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('packages/adminlte/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('packages/adminlte/css/AdminLTE.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('packages/adminlte/css/skins/skin-black.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- Custom Plugins -->
    <link href="{{ asset('backend/css/admin.css') }}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="locale-box">
    <div class="btn-group" role="group">
        @foreach(config('laravellocalization.supportedLocales') as $locale => $param)
            <a class="btn btn-default {{ config('app.locale') === $locale ? 'active' : '' }}" href="{{ config('app.locale') === $locale ? 'javascript:void(0);' : switch_lang($locale) }}">{{ strtoupper($locale) }}</a>
        @endforeach
    </div>
</div>
<div class="login-box">
    <div class="login-logo">
        <a href="{{ route('admin') }}">
            @if(file_exists(public_path('backend/img/logo.png')))
                <img src="{{ asset('backend/img/logo.png') }}" title="{{ trans('base.site_title') }}">
            @else
                <b>{{ trans('base.site_title') }}</b>
            @endif
        </a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        @yield('content')
    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- Admin LTE Plugins -->
<script src="{{ asset('packages/adminlte/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
<script src="{{ asset('packages/adminlte/plugins/jQueryUI/jquery-ui.min.js') }}"></script>
<script src="{{ asset('packages/adminlte/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
<script>
    (function($) {
        $('[type="text"]:first').focus();
    })(jQuery);
</script>

</body>
</html>
