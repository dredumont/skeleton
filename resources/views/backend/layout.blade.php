<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <base href="{{ url() }}" data-current="{{ \Request::url() }}">
    <title>{{ $title }} - {{ trans('base.site_title') }}</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta content="noindex, nofollow" name="robots">
    <meta content="{{ csrf_token() }}" name="csrf-token">

    <!-- Admin LTE Plugins -->
    <link href="{{ asset('packages/adminlte/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('packages/adminlte/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('packages/adminlte/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('packages/adminlte/plugins/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('packages/adminlte/css/AdminLTE.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('packages/adminlte/css/skins/skin-black.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- Custom Plugins -->
    <link href="{{ asset('backend/css/plugins/bootstrap-editable/bootstrap-editable.css') }}" rel="stylesheet"/>
    <link href="{{ asset('backend/css/plugins/colorbox/colorbox.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('backend/css/plugins/datetimepicker/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('backend/css/admin.css') }}" rel="stylesheet" type="text/css" />

    @yield('styles')

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-black fixed sidebar-mini" data-controller="{{ $controller }}">

<!-- Site wrapper -->
<div class="wrapper">

    @include('backend.partials.header')

    <!-- =============================================== -->

    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
        @include('backend.partials.sidebar')
    </aside>

    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        @yield('content')
    </div>
    <!-- /.content-wrapper -->

    @include('backend.partials.footer')

</div>
<!-- ./wrapper -->

<!-- Admin LTE Plugins -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
{{--<script src="{{ asset('packages/adminlte/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>--}}
{{--<script src="{{ asset('packages/adminlte/plugins/jQueryUI/jquery-ui.min.js') }}"></script>--}}
<script src="{{ asset('packages/adminlte/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('packages/adminlte/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('packages/adminlte/plugins/fastclick/fastclick.js') }}"></script>
<script src="{{ asset('packages/adminlte/plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('packages/adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('packages/adminlte/plugins/select2/select2.min.js') }}"></script>
<script src="{{ asset('packages/adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('packages/adminlte/js/app.min.js') }}"></script>

<!-- Custom Plugins -->
<script src="{{ asset('backend/js/plugins/misc/modernizr.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/js/plugins/misc/rails.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/js/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/js/plugins/colorbox/jquery.colorbox-min.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/js/plugins/ckeditor/ckeditor.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/js/plugins/slugify/jquery.slugify.js') }}" type="text/javascript"></script>
<script src="{{ asset('backend/js/plugins/bootstrap-editable/bootstrap-editable.min.js') }}"></script>
<script src="{{ asset('backend/js/plugins/datetimepicker/moment.min.js') }}"></script>
<script src="{{ asset('backend/js/plugins/datetimepicker/moment-with-locales.js') }}"></script>
<script src="{{ asset('backend/js/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('backend/js/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('backend/js/plugins/datepicker/locales/bootstrap-datepicker.' . config('app.locale') . '.js') }}"></script>
<script src="{{ asset('backend/js/admin.js') }}" type="text/javascript"></script>

@yield('scripts')

</body>
</html>
