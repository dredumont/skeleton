@extends('backend.layout')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>{{ $title }}</h1>
    </section>

    <!-- Main content -->
    <section class="content" data-content="{{ explode('@', Route::currentRouteAction())[1] }}">
        <div class="row">
            <div class="col-xs-12 col-sm-9 col-md-8 col-lg-6">
                <div class="box">
                    <div class="box-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if($item->id)
                            {!! Form::open(['route' => ['admin.' . $controller . '.update', $item->id], 'method' => 'PUT', 'autocomplete' => 'off']) !!}
                        @else
                            {!! Form::open(['route' => 'admin.' . $controller . '.store', 'autocomplete' => 'off']) !!}
                        @endif
                            <div class="form-group">
                                {!! Form::label('title', trans('backend.Title')) !!}
                                {!! Form::text('title', $item->title, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('controller', trans('backend.Controller')) !!}
                                {!! Form::text('controller', $item->controller, ['class' => 'form-control']) !!}
                            </div>
                            <hr/>
                            @if($item->id)
                                <div class="form-group">
                                    <button type="button" class="btn btn-sm btn-info btn-add-item" data-ajax-action="{{ route('admin.adminTables.ajax', ['table' => $item->id]) }}">{{ trans('backend.Add a field') }}</button>
                                </div>
                                <div class="form-group">
                                    <div class="panel-group" id="field-items">
                                        <ul class="field-items">
                                            @foreach($item->present()->fields as $fieldItem)
                                                @include('backend.adminTables.partials.fieldItem')
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            @else
                                <div class="form-group">
                                    {{ trans('backend.Save the table once to add fields') }}
                                </div>
                            @endif
                            <hr/>
                            <div class="form-group">
                                {!! Form::submit(trans('backend.Submit'), ['class' => 'btn btn-primary']) !!}
                                <div class="pull-right text-right">
                                    <a href="{{ route('admin.' . $controller) }}" class="cancel btn btn-warning">{{ trans('backend.Cancel') }}</a>
                                    @if($item->id)
                                        <a href="{{ route('admin.' . $controller . '.destroy', ['item' => $item->id]) }}" class="delete btn btn-danger" data-token="{{ csrf_token() }}" data-confirm-text="{{ trans('backend.Are you sure ?') }}">{{ trans('backend.Delete') }}</a>
                                    @endif
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /.content -->

@endsection

@section('scripts')

    <script src="{{ asset('backend/js/plugins/jquery-ace/ace/ace.js') }}"></script>
    <script src="{{ asset('backend/js/plugins/jquery-ace/ace/mode-php.js') }}"></script>
    <script src="{{ asset('backend/js/plugins/jquery-ace/jquery-ace.min.js') }}"></script>
    <script src="{{ asset('backend/js/admin.adminTables.js') }}" type="text/javascript"></script>

@endsection