<li id="field-item-{{ $fieldItem->id }}">
    <div class="panel panel-info">
        <div class="panel-heading">
            <h4 class="panel-title">
                <i class="handle fa fa-arrows"></i>
                <a data-toggle="collapse" data-parent="#field-items" href="#field-item-info-{{ $fieldItem->id }}">{{ $fieldItem->title }}</a>
                <a class="btn-delete-item" href="javascript:void(0);" data-ajax-action="{{ route('admin.adminTables.ajax', ['table' => $item->id]) }}" data-field-item-id="{{ $fieldItem->id }}"><i class="pull-right fa fa-times"></i></a>
            </h4>
        </div>
        <div id="field-item-info-{{ $fieldItem->id }}" class="panel-collapse collapse">
            <input type="hidden" name="field_items[{{ $fieldItem->id }}][id]" value="{{ $fieldItem->id }}">
            <div class="panel-body">
                <div class="form-group">
                    {!! Form::label('title-' . $fieldItem->id, trans('backend.Title')) !!}
                    {!! Form::text('title-' . $fieldItem->id, $fieldItem->title, ['class' => 'form-control', 'name' => 'field_items[' . $fieldItem->id . '][title]']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('field-' . $fieldItem->id, trans('backend.Field')) !!}
                    {!! Form::text('field-' . $fieldItem->id, $fieldItem->field, ['class' => 'form-control', 'name' => 'field_items[' . $fieldItem->id . '][field]']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('callback-' . $fieldItem->id, trans('backend.Callback')) !!}
                    {!! Form::textarea('callback-' . $fieldItem->id, $fieldItem->callback, ['class' => 'form-control code', 'name' => 'field_items[' . $fieldItem->id . '][callback]', 'rows' => 5, 'style' => 'width: 100%']) !!}
                </div>
            </div>
        </div>
    </div>
</li>
