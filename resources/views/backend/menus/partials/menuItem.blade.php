<li id="menu-item-{{ $menuItem->id }}">
    <div class="panel panel-info">
        <div class="panel-heading">
            <h4 class="panel-title">
                <i class="handle fa fa-arrows"></i>
                <a data-toggle="collapse" data-parent="#menu-items-{{ $locale }}" href="#menu-item-{{ $menuItem->id }}-{{ $locale }}">{{ $menuItem->title }}</a>
                <a class="btn-delete-item" href="javascript:void(0);" data-ajax-action="{{ route('admin.menus.ajax', ['menu' => $item->id]) }}" data-menu-item-id="{{ $menuItem->id }}"><i class="pull-right fa fa-times"></i></a>
            </h4>
        </div>
        <div id="menu-item-{{ $menuItem->id }}-{{ $locale }}" class="panel-collapse collapse">
            <input type="hidden" name="menu_items[{{ $menuItem->id }}][locale]" value="{{ $locale }}">
            <div class="panel-body">
                <div class="form-group">
                    {!! Form::label('title-' . $menuItem->id . '-' . $locale, trans('backend.Title')) !!}
                    {!! Form::text('title-' . $menuItem->id . '-' . $locale, $menuItem->title, ['class' => 'form-control', 'name' => 'menu_items[' . $menuItem->id . '][title]']) !!}
                </div>
                @if($menuItem->is_link)
                    <input type="hidden" name="menu_items[{{ $menuItem->id }}][is_link]" value="1">
                    <div class="form-group">
                        {!! Form::label('url-' . $menuItem->id . '-' . $locale, trans('backend.Url')) !!}
                        {!! Form::text('url-' . $menuItem->id . '-' . $locale, $menuItem->value, ['class' => 'form-control', 'name' => 'menu_items[' . $menuItem->id . '][value]']) !!}
                    </div>
                @endif
                @if($menuItem->is_page)
                    <input type="hidden" name="menu_items[{{ $menuItem->id }}][is_page]" value="1">
                    <div class="form-group">
                        {!! Form::label('page-' . $menuItem->id . '-' . $locale, trans('backend.Page')) !!}
                        <select class="form-control select2" name="menu_items[{{ $menuItem->id }}][value]" id="{{ 'url-' . $menuItem->id . '-' . $locale }}">
                            <option value="">{{ trans('backend.None') }}</option>
                            @foreach(\App\Models\Page::where('locale', '')->orderBy('title')->get() as $page)
                                <option value="{{ $page->id }}" {{ old('value', $menuItem->value) == $page->id ? 'selected' : '' }}>{{ $page->title }}</option>
                            @endforeach
                            <optgroup label="{{ strtoupper($locale) }}"></optgroup>
                            @foreach(\App\Models\Page::where('locale', $locale)->orderBy('title')->get() as $page)
                                <option value="{{ $page->id }}" {{ old('value', $menuItem->value) == $page->id ? 'selected' : '' }}>{{ $page->title }}</option>
                            @endforeach
                        </select>
                    </div>
                @endif
                <div class="form-group">
                    {!! Form::label('target-' . $menuItem->id . '-' . $locale, trans('backend.Target')) !!}
                    {!! Form::select('target-' . $menuItem->id . '-' . $locale, $targetList, old('target', $menuItem->target), ['class' => 'form-control', 'name' => 'menu_items[' . $menuItem->id . '][target]']) !!}
                </div>
                <div class="form-group">
                    <input type="hidden" name="menu_items[{{ $menuItem->id }}][active]" value="0">
                    {!! Form::checkbox('menu_items[' . $menuItem->id . '][active]', 1, $menuItem->active, ['class' => 'simple', 'id' => 'active-' . $menuItem->id . '-' . $locale]) !!}
                    {!! Form::label('active-' . $menuItem->id . '-' . $locale, trans('backend.Active')) !!}
                </div>
            </div>
        </div>
    </div>
    @if(is_numeric($menuItem->id) && $menuItem->children)
        <ul>
            @foreach($menuItem->children()->where('locale', '=', $locale)->orderBy('position', 'ASC')->get() as $menuItem)
                @include('backend.menus.partials.menuItem')
            @endforeach
        </ul>
    @endif
</li>
