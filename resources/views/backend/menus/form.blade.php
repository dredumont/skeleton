@extends('backend.layout')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>{{ $title }}</h1>
    </section>

    <!-- Main content -->
    <section class="content" data-content="{{ explode('@', Route::currentRouteAction())[1] }}">
        <div class="row">
            <div class="col-xs-12 col-sm-9 col-md-8 col-lg-6">
                <div class="box">
                    <div class="box-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if($item->id)
                        {!! Form::open(['route' => ['admin.' . $controller . '.update', $item->id], 'method' => 'PUT', 'autocomplete' => 'off']) !!}
                        @else
                        {!! Form::open(['route' => 'admin.' . $controller . '.store', 'autocomplete' => 'off']) !!}
                        @endif
                            <div class="form-group">
                                {!! Form::label('title', trans('backend.Title')) !!}
                                {!! Form::text('title', $item->title, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('ident', trans('backend.Ident')) !!}
                                {!! Form::text('ident', $item->ident, ['class' => 'form-control']) !!}
                            </div>
                            <hr/>
                                @if($item->id)
                                    <div role="tabpanel">
                                        <input type="hidden" name="menu_items_delete" value="">
                                        <ul class="nav nav-tabs" role="tablist">
                                            @foreach(config('laravellocalization.supportedLocales') as $locale => $param)
                                                <li role="presentation" class="{{ $locale == LaravelLocalization::getDefaultLocale() ? 'active' : '' }}">
                                                    <a href="#{{ $locale }}" aria-controls="{{ $locale }}" role="tab" data-toggle="tab">{{ $param['name'] }}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                        <br>
                                        <div class="tab-content">
                                            @foreach(config('laravellocalization.supportedLocales') as $locale => $param)
                                                <div role="tabpanel" class="tab-pane {{ $locale == LaravelLocalization::getDefaultLocale() ? 'active' : '' }}" id="{{ $locale }}">
                                                    <div class="form-group">
                                                        <button type="button" class="btn btn-sm btn-info btn-add-item" data-item-type="link" data-ajax-action="{{ route('admin.menus.ajax', ['menu' => $item->id]) }}" data-locale="{{ $locale }}">{{ trans('backend.Add a link') }}</button>
                                                        <button type="button" class="btn btn-sm btn-info btn-add-item" data-item-type="page" data-ajax-action="{{ route('admin.menus.ajax', ['menu' => $item->id]) }}" data-locale="{{ $locale }}">{{ trans('backend.Add a page') }}</button>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="panel-group" id="menu-items-{{ $locale }}">
                                                            <input type="hidden" name="menu_items_tree_{{ $locale }}" value="">
                                                            <ul class="menu-items">
                                                                @foreach($item->items()->where('locale', '=', $locale)->where('parent_id', '=', '0')->orderBy('position', 'ASC')->get() as $menuItem)
                                                                    @include('backend.menus.partials.menuItem')
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                @else
                                    <div class="form-group">
                                        {{ trans('backend.Save the menu once to add links') }}
                                    </div>
                                @endif
                            <hr/>
                            <div class="form-group">
                                <input type="hidden" name="active" value="0">
                                {!! Form::checkbox('active', 1, $item->active, ['class' => 'simple', 'id' => 'active']) !!}
                                {!! Form::label('active', trans('backend.Active')) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::submit(trans('backend.Submit'), ['class' => 'btn btn-primary']) !!}
                                <div class="pull-right text-right">
                                    <a href="{{ route('admin.' . $controller) }}" class="cancel btn btn-warning">{{ trans('backend.Cancel') }}</a>
                                    @if($item->id)
                                        <a href="{{ route('admin.' . $controller . '.destroy', ['item' => $item->id]) }}" class="delete btn btn-danger" data-token="{{ csrf_token() }}" data-confirm-text="{{ trans('backend.Are you sure ?') }}">{{ trans('backend.Delete') }}</a>
                                    @endif
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /.content -->

@endsection

@section('scripts')

    <script src="{{ asset('backend/js/plugins/nestedSortable/jquery.nestedSortable.js') }}"></script>
    <script src="{{ asset('backend/js/admin.menus.js') }}" type="text/javascript"></script>

@endsection