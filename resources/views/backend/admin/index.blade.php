@extends('backend.layout')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>{{ $title }}</h1>
    </section>

    <!-- Main content -->
    <section class="content" data-content="{{ explode('@', Route::currentRouteAction())[1] }}">
        <div class="box">
            <div class="box-body">
                ...
            </div>
        </div>
    </section><!-- /.content -->

@endsection