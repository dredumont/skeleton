@extends('backend.layout')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>{{ $title }}</h1>
    </section>

    <!-- Main content -->
    <section class="content" data-content="{{ explode('@', Route::currentRouteAction())[1] }}">
        <div class="box">
            <div class="box-body">
                <table id="table" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th class="text-center">
                                <input type="checkbox" class="simple select-all">
                            </th>
                            @foreach($fieldList as $field)
                                <th>{{ trans('backend.' . $field['title']) }}</th>
                            @endforeach
                            <th>{{ trans('backend.Actions') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($itemList as $item)
                            <tr>
                                <td class="text-center">
                                    <input type="checkbox" class="simple select-single">
                                </td>
                                @foreach($fieldList as $key => $field)
                                    @if(isset($field['callback']))
                                        <td>{!! $field['callback']($item->$key, $item) !!}</td>
                                    @else
                                        <td>{{ $item->$key }}</td>
                                    @endif
                                @endforeach
                                <td>
                                    <a href="{{ route('admin.' . $controller . '.edit', [$controller => $item->id]) }}" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i> {{ trans('backend.Edit') }}</a>
                                    <a href="{{ route('admin.' . $controller . '.destroy', [$controller => $item->id]) }}" class="delete btn btn-danger btn-xs" data-confirm-text="{{ trans('backend.Are you sure ?') }}"><i class="fa fa-trash"></i> {{ trans('backend.Delete') }}</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th class="text-center">
                                <a href="javascript:void(0);" class="delete-selected btn btn-danger btn-xs"><i class="fa fa-trash"></i> {{ trans('backend.Delete') }}</a>
                            </th>
                            <th colspan="{{ count($fieldList) + 1 }}"></th>
                        </tr>
                    </tfoot>
                </table>
                <div class="row">
                    <div class="col-md-12">
                        <a href="{{ route('admin.' . $controller . '.create') }}" class="btn btn-primary">{{ trans('backend.Create') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /.content -->

@endsection