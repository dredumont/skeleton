@extends('backend.layout')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>{{ $title }}</h1>
    </section>

    <!-- Main content -->
    <section class="content" data-content="{{ explode('@', Route::currentRouteAction())[1] }}">
        <div class="row">
            <div class="col-xs-12 col-sm-9 col-md-8 col-lg-6">
                <div class="box">
                    <div class="box-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if($item->id)
                            {!! Form::open(['route' => ['admin.' . $controller . '.update', $item->id], 'method' => 'PUT', 'autocomplete' => 'off']) !!}
                        @else
                            {!! Form::open(['route' => 'admin.' . $controller . '.store', 'autocomplete' => 'off']) !!}
                        @endif
                            <div class="form-group">
                                {!! Form::label('name', trans('backend.Name')) !!}
                                {!! Form::text('name', $item->name, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('description', trans('backend.Description')) !!}
                                {!! Form::textarea('description', $item->description, ['class' => 'form-control', 'rows' => 3]) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::submit(trans('backend.Submit'), ['class' => 'btn btn-primary']) !!}
                                <div class="pull-right text-right">
                                    <a href="{{ route('admin.' . $controller) }}" class="cancel btn btn-warning">{{ trans('backend.Cancel') }}</a>
                                    @if($item->id)
                                        <a href="{{ route('admin.' . $controller . '.destroy', ['item' => $item->id]) }}" class="delete btn btn-danger" data-token="{{ csrf_token() }}" data-confirm-text="{{ trans('backend.Are you sure ?') }}">{{ trans('backend.Delete') }}</a>
                                    @endif
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /.content -->

@endsection