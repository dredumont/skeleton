<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>{{ trans('passwords.Reset your password') }}</title>
    <style type="text/css" rel="stylesheet" media="all">
        @import url(https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic);
        /* -------------------------------------
            GLOBAL
        ------------------------------------- */
        * {
            margin: 0;
            padding: 0;
            font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
            box-sizing: border-box;
            font-size: 14px;
            color: #666;
        }

        body {
            -webkit-font-smoothing: antialiased;
            -webkit-text-size-adjust: none;
            width: 100% !important;
            height: 100%;
            line-height: 1.6;
        }

        /* Let's make sure all tables have defaults */
        table td {
            vertical-align: top;
        }

        /* -------------------------------------
            BODY & CONTAINER
        ------------------------------------- */
        body {
            background-color: #d2d6de;
        }

        .body-wrap {
            background-color: #d2d6de;
            width: 100%;
        }

        .container {
            display: block !important;
            max-width: 600px !important;
            margin: 0 auto !important;
            /* makes it centered */
            clear: both !important;
        }

        .content {
            max-width: 600px;
            margin: 0 auto;
            display: block;
            padding: 20px;
        }

        /* -------------------------------------
            HEADER, FOOTER, MAIN
        ------------------------------------- */
        .main {
            background: #fff;
        }

        .content-wrap {
            padding: 20px;
        }

        .content-block {
            padding: 0 0 20px;
        }

        /* -------------------------------------
            TYPOGRAPHY
        ------------------------------------- */
        h1, h2, h3 {
            line-height: 1.2;
            font-weight: 400;
        }

        h1 {
            font-size: 32px;
            font-weight: 500;
        }

        h2 {
            font-size: 24px;
        }

        h3 {
            font-size: 18px;
        }

        h4 {
            font-size: 14px;
            font-weight: 600;
        }

        /* -------------------------------------
            LINKS & BUTTONS
        ------------------------------------- */
        a {
            color: #3c8dbc;
            text-decoration: underline;
        }

        .btn-primary {
            text-decoration: none;
            color: #FFF;
            background-color: #3c8dbc;
            border: solid #3c8dbc;
            border-width: 5px 15px;
            line-height: 2;
            text-align: center;
            cursor: pointer;
            display: inline-block;
        }

        /* -------------------------------------
            RESPONSIVE AND MOBILE FRIENDLY STYLES
        ------------------------------------- */
        @media only screen and (max-width: 640px) {
            h1, h2, h3, h4 {
                font-weight: 600 !important;
                margin: 20px 0 5px !important;
            }

            h1 {
                font-size: 22px !important;
            }

            h2 {
                font-size: 18px !important;
            }

            h3 {
                font-size: 16px !important;
            }

            .container {
                width: 100% !important;
            }

            .content {
                padding: 10px !important;
            }
        }
    </style>
</head>
<body>

<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" width="600">
            <div class="content">
                <table class="main" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="content-wrap">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="content-block">
                                        <h2>{{ trans('passwords.reset_email_1', ['firstname' => $user->firstname]) }}</h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content-block">
                                        {{ trans('passwords.reset_email_2') }}
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content-block">
                                        <a href="{{ route('admin.password.getReset', [$token]) }}" class="btn-primary">{{ trans('passwords.Reset your password') }}</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content-block">
                                        {{ trans('passwords.reset_email_3') }}
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content-block">
                                        {{ trans('passwords.reset_email_4') }} <a href="{{ route('site.home') }}">{{ trans('passwords.reset_email_5') }}</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content-block"></td>
                                </tr>
                                <tr>
                                    <td class="content-block">
                                        <h3>- {{ trans('base.site_title') }}</h3>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
        <td></td>
    </tr>
</table>

</body>
</html>