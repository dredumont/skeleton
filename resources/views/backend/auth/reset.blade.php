@extends('backend.auth')

@section('content')

    <form method="post" autocomplete="off" novalidate="novalidate" action="{{ route('admin.password.postReset') }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="token" value="{{ $token }}">

        <p class="login-box-msg">{{ trans('passwords.Enter your email and your new password') }}</p>
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <p><i class="fa fa-angle-right"></i> {{ $error }}</p>
                @endforeach
            </div>
        @endif
        <div class="form-group has-feedback">
            <input type="text" name="email" class="form-control" placeholder="{{ trans('backend.Email') }}" value="{{ old('email') }}">
            <i class="fa fa-envelope form-control-feedback"></i>
        </div>
        <div class="form-group has-feedback">
            <input type="password" name="password" class="form-control" placeholder="{{ trans('backend.Password') }}">
            <i class="fa fa-lock form-control-feedback"></i>
        </div>
        <div class="form-group has-feedback">
            <input type="password" name="password_confirmation" class="form-control" placeholder="{{ trans('backend.Password Confirmation') }}">
            <i class="fa fa-lock form-control-feedback"></i>
        </div>
        <div class="row">
            <div class="col-xs-12 text-center">
                <button type="submit" class="btn btn-primary btn-flat">{{ trans('passwords.Reset your password') }}</button>
            </div>
            <!-- /.col -->
        </div>
    </form>

@endsection
