@extends('backend.auth')

@section('content')

    <form method="post" autocomplete="off" novalidate="novalidate">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <p class="login-box-msg">{{ trans('backend.Sign in to start your session') }}</p>
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <p><i class="fa fa-angle-right"></i> {{ $error }}</p>
                @endforeach
            </div>
        @endif
        <div class="form-group has-feedback">
            <input type="text" name="username" class="form-control" placeholder="{{ trans('backend.Username') }}" value="{{ old('username') }}">
            <i class="fa fa-user form-control-feedback"></i>
        </div>
        <div class="form-group has-feedback">
            <input type="password" name="password" class="form-control" placeholder="{{ trans('backend.Password') }}">
            <i class="fa fa-lock form-control-feedback"></i>
        </div>
        <div class="row">
            <div class="col-xs-8">
                <div class="checkbox">
                    <label>
                        <input type="checkbox"> {{ trans('backend.Remember Me') }}
                    </label>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-xs-4">
                <button type="submit" class="btn btn-primary btn-block btn-flat">{{ trans('backend.Sign In') }}</button>
            </div>
            <!-- /.col -->
        </div>
    </form>
    <a href="{{ route('admin.password.getEmail') }}">{{ trans('backend.Forgot your password') }}</a>
    @if(env('SOCIAL_LOGIN'))
    <div class="social-auth-links text-center">
        <hr>
        @if(env('GOOGLE_LOGIN'))
        <a href="{{ route('admin.googleLogin') }}" class="btn btn-block btn-social btn-google btn-flat">
            <i class="fa fa-google"></i> Sign in using Google
        </a>
        @endif
        @if(env('FACEBOOK_LOGIN'))
        <a href="{{ route('admin.facebookLogin') }}" class="btn btn-block btn-social btn-facebook btn-flat">
            <i class="fa fa-facebook"></i> Sign in using Facebook
        </a>
        @endif
    </div>
    @endif

@endsection
