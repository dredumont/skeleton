@extends('backend.layout')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>{{ $title }}</h1>
    </section>

    <!-- Main content -->
    <section class="content" data-content="{{ explode('@', Route::currentRouteAction())[1] }}">
        <div class="row">
            <div class="col-xs-12 col-sm-9 col-md-8 col-lg-6">
                <div class="box">
                    <div class="box-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if($item->id)
                            {!! Form::open(['route' => ['admin.' . $controller . '.update', $item->id], 'method' => 'PUT', 'autocomplete' => 'off']) !!}
                        @else
                            {!! Form::open(['route' => 'admin.' . $controller . '.store', 'autocomplete' => 'off']) !!}
                        @endif
                            <div class="form-group">
                                {!! Form::label('firstname', trans('backend.Firstname')) !!}
                                {!! Form::text('firstname', $item->firstname, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('lastname', trans('backend.Lastname')) !!}
                                {!! Form::text('lastname', $item->lastname, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('email', trans('backend.Email')) !!}
                                {!! Form::text('email', $item->email, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('username', trans('backend.Username')) !!}
                                {!! Form::text('username', $item->username, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('password', trans('backend.Password')) !!}
                                {!! Form::password('password', ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('password_confirmation', trans('backend.Password Confirmation')) !!}
                                {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('roles', trans('backend.Roles')) !!}
                                {!! Form::select('roles', $roleList, $item->roles()->lists('role_id'), ['class' => 'form-control select2', 'name' => 'roles[]', 'multiple' => true]) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('avatar', trans('backend.Avatar')) !!}
                                {!! Form::adminImage('avatar', $controller, $item) !!}
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="confirmed" value="0">
                                {!! Form::checkbox('confirmed', 1, $item->confirmed, ['class' => 'simple', 'id' => 'confirmed']) !!}
                                {!! Form::label('confirmed', trans('backend.Active')) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::submit(trans('backend.Submit'), ['class' => 'btn btn-primary']) !!}
                                <div class="pull-right text-right">
                                    <a href="{{ route('admin.' . $controller) }}" class="cancel btn btn-warning">{{ trans('backend.Cancel') }}</a>
                                    @if($item->id)
                                        <a href="{{ route('admin.' . $controller . '.destroy', ['item' => $item->id]) }}" class="delete btn btn-danger" data-confirm-text="{{ trans('backend.Are you sure ?') }}">{{ trans('backend.Delete') }}</a>
                                    @endif
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /.content -->

@endsection