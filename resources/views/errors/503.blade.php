@extends('frontend.layout')

@section('content')

    <section>
        <h1>{{ trans('base.maintenance_mode') }}</h1>
    </section>

@endsection