<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('active');
            $table->integer('position');
            $table->string('title');
            $table->string('slug');
            $table->text('content');
            $table->text('url');
            $table->text('meta_title');
            $table->text('meta_description');
            $table->text('image');
            $table->string('ident');
            $table->string('locale');
            $table->string('template');
            $table->integer('parent_id')->unsigned()->index();
            $table->dateTime('publish_date');
            $table->dateTime('expiry_date');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pages');
    }
}
