<?php

use Illuminate\Database\Seeder;
use App\Models\Menu;

class MenusTableSeeder extends Seeder
{
    public function run()
    {
        Menu::create([
            'id'        => '1',
            'active'    => '1',
            'title'     => 'Menu principal',
            'ident'     => 'main_menu',
        ]);
    }
}
