<?php

use Illuminate\Database\Seeder;
use App\Models\AdminTable;

class AdminTablesTableSeeder extends Seeder
{
    public function run()
    {
        AdminTable::create([
            'id'            => '1',
            'title'         => 'Pages',
            'controller'    => 'pages',
            'fields'        => '{
                "b989027705edb171f40f82c3b9ba6979":{
                    "id":"b989027705edb171f40f82c3b9ba6979",
                    "title":"Title",
                    "field":"title",
                    "callback":"<?php\r\nreturn $value;"
                },
                "451eee68e602228928fcfa17514bed47":{
                    "id":"451eee68e602228928fcfa17514bed47",
                    "title":"Url",
                    "field":"url",
                    "callback":"<?php\r\nreturn $value;"
                },
                "d0975cb7438890ddc249ffd846b242c3":{
                    "id":"d0975cb7438890ddc249ffd846b242c3",
                    "title":"Locale",
                    "field":"locale",
                    "callback":"<?php\r\nif ($value) {\r\n    return config(\'laravellocalization.supportedLocales\')[$value][\'name\'];\r\n}"
                },
                "03694c49d956a9a80f5a2836d6817d63":{
                    "id":"03694c49d956a9a80f5a2836d6817d63",
                    "title":"Active",
                    "field":"active",
                    "callback":"<?php\r\nreturn $value ? trans(\'backend.Yes\') : trans(\'backend.No\');"
                }
            }',
        ]);

        AdminTable::create([
            'id'            => '2',
            'title'         => 'Menus',
            'controller'    => 'menus',
            'fields'        => '{
                "a3b9d71a5e77cee788f606c6ace8d386":{
                    "id":"a3b9d71a5e77cee788f606c6ace8d386",
                    "title":"Title",
                    "field":"title",
                    "callback":"<?php\r\nreturn $value;"
                },
                "4bb46d004756c90ee2d063c6f03a3150":{
                    "id":"4bb46d004756c90ee2d063c6f03a3150",
                    "title":"Ident",
                    "field":"ident",
                    "callback":"<?php\r\nreturn $value;"
                },
                "1800461041bf71baadb954facce8c02d":{
                    "id":"1800461041bf71baadb954facce8c02d",
                    "title":"Active",
                    "field":"active",
                    "callback":"<?php\r\nreturn $value ? trans(\'backend.Yes\') : trans(\'backend.No\');"
                }
            }',
        ]);

        AdminTable::create([
            'id'            => '4',
            'title'         => 'Roles',
            'controller'    => 'roles',
            'fields'        => '{
                "e03787395c81f531ae931f1388d8425d":{
                    "id":"e03787395c81f531ae931f1388d8425d",
                    "title":"Name",
                    "field":"name",
                    "callback":"<?php\r\nreturn $value;"},
                "c849d7ca8a2adc6340bbefe76d148894":{
                    "id":"c849d7ca8a2adc6340bbefe76d148894",
                    "title":"Description",
                    "field":"description",
                    "callback":"<?php\r\nif (strlen($value) > 50) {\r\n    return \\\Stringy\\\StaticStringy::safeTruncate($value, 50) . \'...\';\r\n}\r\nreturn $value;"
                },
                "8bc6e2f14e5e5982ba9321ff0a4e35e6":{
                    "id":"8bc6e2f14e5e5982ba9321ff0a4e35e6",
                    "title":"Level",
                    "field":"level",
                    "callback":"<?php\r\nreturn $value;"
                }
            }',
        ]);

        AdminTable::create([
            'id'            => '5',
            'title'         => 'Users',
            'controller'    => 'users',
            'fields'        => '{
                "ddd4ec58a5c1354e6bf7e7d52069c520":{
                    "id":"ddd4ec58a5c1354e6bf7e7d52069c520",
                    "title":"Username",
                    "field":"username",
                    "callback":"<?php\r\nreturn $value;"
                },
                "3eaa542948f415223e65b53f2f8fda5a":{
                    "id":"3eaa542948f415223e65b53f2f8fda5a",
                    "title":"Email",
                    "field":"email",
                    "callback":"<?php\r\nreturn $value;"
                },
                "76234cf84bc277a70785c6aa09f74dcc":{
                    "id":"76234cf84bc277a70785c6aa09f74dcc",
                    "title":"Firstname",
                    "field":"firstname",
                    "callback":"<?php\r\nreturn $value;"
                },
                "b64d46eb0f73f802dc089f3a5254931c":{
                    "id":"b64d46eb0f73f802dc089f3a5254931c",
                    "title":"Lastname",
                    "field":"lastname",
                    "callback":"<?php\r\nreturn $value;"
                },
                "fc4ebf49c5f085182b75232ff8115203":{
                    "id":"fc4ebf49c5f085182b75232ff8115203",
                    "title":"Roles",
                    "field":"roles",
                    "callback":"<?php\r\n$valueArray = [];\r\nforeach ($value as $role) {\r\n    array_push($valueArray, $role->name);\r\n}\r\nreturn implode(\', \', $valueArray);"
                },
                "31841e17d0ae2d8da9711d4d3d67ed62":{
                    "id":"31841e17d0ae2d8da9711d4d3d67ed62",
                    "title":"Active",
                    "field":"confirmed",
                    "callback":"<?php\r\nreturn $value ? trans(\'backend.Yes\') : trans(\'backend.No\');"
                }
            }',
        ]);

        AdminTable::create([
            'id'            => '6',
            'title'         => 'Admin Tables',
            'controller'    => 'adminTables',
            'fields'        => '{
                "019b4f9295b0c79ec130ab9f04a92d67":{
                    "id":"019b4f9295b0c79ec130ab9f04a92d67",
                    "title":"Title",
                    "field":"title",
                    "callback":"<?php\r\nreturn $value;"
                },
                "5e48a02a59dd5cc951c0ea586475daec":{
                    "id":"5e48a02a59dd5cc951c0ea586475daec",
                    "title":"Controller",
                    "field":"controller",
                    "callback":"<?php\r\nreturn $value;"
                }
            }',
        ]);

    }
}
