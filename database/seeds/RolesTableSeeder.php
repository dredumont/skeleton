<?php

use Illuminate\Database\Seeder;
use Bican\Roles\Models\Role;

class RolesTableSeeder extends Seeder
{
    public function run()
    {
        Role::create([
            'name'  => 'Super Admin',
            'slug'  => 'super.admin',
            'level' => '5',
        ]);

        Role::create([
            'name'  => 'Admin',
            'slug'  => 'admin',
            'level' => '4',
        ]);

        Role::create([
            'name'  => 'User',
            'slug'  => 'user',
            'level' => '1',
        ]);
    }
}
