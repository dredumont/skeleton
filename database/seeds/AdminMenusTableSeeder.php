<?php

use Illuminate\Database\Seeder;
use App\Models\AdminMenu;

class AdminMenusTableSeeder extends Seeder
{
    public function run()
    {
        AdminMenu::create([
            'id'        => '1',
            'active'    => '1',
            'position'  => '2',
            'title'     => 'Pages',
            'route'     => 'admin.pages',
            'icon'      => 'fa-file',
            'parent_id' => '0',
        ]);

        AdminMenu::create([
            'id'        => '3',
            'active'    => '1',
            'position'  => '3',
            'title'     => 'Menus',
            'route'     => 'admin.menus',
            'icon'      => 'fa-bars',
            'parent_id' => '0',
        ]);

        AdminMenu::create([
            'id'        => '4',
            'active'    => '1',
            'position'  => '1',
            'title'     => 'Home',
            'route'     => 'admin',
            'icon'      => 'fa-home',
            'parent_id' => '0',
        ]);

        AdminMenu::create([
            'id'        => '5',
            'active'    => '1',
            'position'  => '4',
            'title'     => 'Images',
            'route'     => 'admin.images',
            'icon'      => 'fa-image',
            'parent_id' => '0',
        ]);

        AdminMenu::create([
            'id'        => '6',
            'active'    => '1',
            'position'  => '5',
            'title'     => 'Files',
            'route'     => 'admin.files',
            'icon'      => 'fa-files-o',
            'parent_id' => '0',
        ]);

        AdminMenu::create([
            'id'                => '7',
            'active'            => '1',
            'position'          => '6',
            'title'             => 'Translations',
            'route'             => 'admin.translations',
            'icon'              => 'fa-font',
            'parent_id'         => '0',
            'super_admin_only'  => '1',
        ]);

        AdminMenu::create([
            'id'                => '8',
            'active'            => '1',
            'position'          => '7',
            'title'             => 'Users',
            'route'             => 'admin.users',
            'icon'              => 'fa-user',
            'parent_id'         => '0',
            'super_admin_only'  => '1',
        ]);

        AdminMenu::create([
            'id'                => '9',
            'active'            => '1',
            'position'          => '1',
            'title'             => 'Users',
            'route'             => 'admin.users',
            'icon'              => '',
            'parent_id'         => '8',
            'super_admin_only'  => '1',
        ]);

        AdminMenu::create([
            'id'                => '10',
            'active'            => '1',
            'position'          => '2',
            'title'             => 'Roles',
            'route'             => 'admin.roles',
            'icon'              => '',
            'parent_id'         => '8',
            'super_admin_only'  => '1',
        ]);

        AdminMenu::create([
            'id'                => '12',
            'active'            => '1',
            'position'          => '8',
            'title'             => 'Administration',
            'route'             => 'admin.adminMenus',
            'icon'              => 'fa-lock',
            'parent_id'         => '0',
            'super_admin_only'  => '1',
        ]);

        AdminMenu::create([
            'id'                => '13',
            'active'            => '1',
            'position'          => '1',
            'title'             => 'Menus',
            'route'             => 'admin.adminMenus',
            'icon'              => '',
            'parent_id'         => '12',
            'super_admin_only'  => '1',
        ]);

        AdminMenu::create([
            'id'                => '14',
            'active'            => '1',
            'position'          => '2',
            'title'             => 'Tables',
            'route'             => 'admin.adminTables',
            'icon'              => '',
            'parent_id'         => '12',
            'super_admin_only'  => '1',
        ]);

    }
}
