<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    /**
     * @var string
     */
    protected $table = 'menus';
    /**
     * @var array
     */
    protected $fillable = ['active', 'title', 'ident'];

    public function items()
    {
        return $this->hasMany('App\Models\MenuItem');
    }
}
