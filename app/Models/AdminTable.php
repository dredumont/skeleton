<?php namespace App\Models;

use Pingpong\Presenters\Model;

class AdminTable extends Model
{
    /**
     * @var string
     */
    protected $table = 'admin_tables';
    /**
     * @var array
     */
    protected $guarded = ['id', 'field_items', 'created_at', 'updated_at'];
    /**
     * @var string
     */
    protected $presenter = 'App\Models\Presenters\AdminTablePresenter';
}
