<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Translation extends Model
{
    /**
     *
     */
    const STATUS_SAVED = 0;
    /**
     *
     */
    const STATUS_CHANGED = 1;

    /**
     * @var string
     */
    protected $table = 'translations';
    /**
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
