<?php namespace App\Models\Presenters;

use Pingpong\Presenters\Presenter;

class AdminTablePresenter extends Presenter
{
    public function fields()
    {
        if ($this->entity->fields) {
            return json_decode($this->entity->fields);
        }

        return [];
    }
}
