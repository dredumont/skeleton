<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Bican\Roles\Contracts\HasRoleAndPermissionContract;
use Bican\Roles\Traits\HasRoleAndPermission;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract, HasRoleAndPermissionContract
{
    use SoftDeletes, Authenticatable, CanResetPassword, HasRoleAndPermission;

    /**
     * @var string
     */
    protected $table = 'users';
    /**
     * @var array
     */
    protected $guarded = ['id', 'roles', 'password_confirmation', 'created_at', 'updated_at', 'deleted_at'];
    /**
     * @var array
     */
    protected $hidden = ['password'];
    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Hash password before database entry
     *
     * @param $password
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = \Hash::make($password);
    }
}
