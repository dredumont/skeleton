<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminMenu extends Model
{
    /**
     * @var string
     */
    protected $table = 'admin_menus';
    /**
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function children()
    {
        return $this->hasMany('App\Models\AdminMenu', 'parent_id');
    }
}
