<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuItem extends Model
{
    /**
     * @var string
     */
    protected $table = 'menu_items';
    /**
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function children()
    {
        return $this->hasMany('App\Models\MenuItem', 'parent_id');
    }
}
