<?php namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;

class ReptileProtectServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return mixed
     */
    public function boot()
    {
        if (!getenv('REPTILE_PROTECT') || getenv('REPTILE_PROTECT') == 'false') {
            return null;
        }

        $reptileProtectData  = 'http://protect.reptiletech.com/reptile_protect.json';
        $cacheKey           = 'reptileProtectLastUpdated';
        $dateFormat         = 'Y-m-d H:i:s';
        $now                = date($dateFormat);

        // Get last updated otherwise store it
        $lastUpdated = \Cache::rememberForever($cacheKey, function () use ($now) {
            return $now;
        });

        // Create Carbon dates and calculate if it's time to run
        $nowDate            = Carbon::createFromFormat($dateFormat, $now);
        $lastUpdatedDate    = Carbon::createFromFormat($dateFormat, $lastUpdated);

        if ($nowDate->diffInMinutes($lastUpdatedDate) < getenv('REPTILE_PROTECT_FREQUENCE')) {
            return null;
        }

        // Store new last updated
        \Cache::forever($cacheKey, $now);

        $file = @file_get_contents($reptileProtectData);
        if (!$file) {
            return \Artisan::call('up');
        }

        $obj = json_decode($file);
        $clients = $obj->reptile_protect->clients;
        if (!isset($clients->{getenv('REPTILE_PROTECT_NAME')})) {
            return \Artisan::call('up');
        }

        $status = $clients->{getenv('REPTILE_PROTECT_NAME')};
        if ($status === 'blocked') {
            return \Artisan::call('down');
        } else {
            return \Artisan::call('up');
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
