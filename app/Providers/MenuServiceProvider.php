<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Menu;
use App\Models\MenuItem;
use App\Models\Page;

class MenuServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (!\Schema::hasTable('menus')) {
            return;
        }

        foreach (Menu::where('active', '=', true)->get() as $menu) {
            \PingPongMenu::create($menu->ident, function ($m) use ($menu) {
                $menuItemList = MenuItem::where('active', '=', true)
                    ->where('locale', '=', config('app.locale'))
                    ->where('menu_id', '=', $menu->id)
                    ->where('parent_id', '=', 0)
                    ->orderBy('position', 'ASC')
                    ->get();

                foreach ($menuItemList as $menuItem) {
                    $this->getLoopMenuItem($m, $menuItem, true);
                }
            });
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Recursively add item to the menu
     *
     * @param $m
     * @param $menuItem
     * @param $isParent
     * @return void
     */
    protected function getLoopMenuItem($m, $menuItem, $isParent = false)
    {
        $menuItemsIds = $menuItem->children->lists('id');
        if ($menuItem->children) {
            $subMenuItemList = MenuItem::where('active', '=', true)
                ->whereIn('id', $menuItemsIds)
                ->where('locale', '=', config('app.locale'))
                ->orderBy('position', 'ASC')
                ->get();

            if ($isParent && count($subMenuItemList)) {
                $m->dropdown($menuItem->title, function ($sub) use ($subMenuItemList) {
                    foreach ($subMenuItemList as $subMenuItem) {
                        $this->getLoopMenuItem($sub, $subMenuItem);
                    }
                });
            } else {
                if (count($subMenuItemList)) {
                    $m->dropdown($menuItem->title, 0, function ($sub) use ($subMenuItemList) {
                        foreach ($subMenuItemList as $subMenuItem) {
                            $this->getLoopMenuItem($sub, $subMenuItem);
                        }
                    });
                } else {
                    $m->url(
                        $this->getLinkValueByType($menuItem),
                        $menuItem->title,
                        null,
                        ['target' => $menuItem->target]
                    );
                }
            }
        } else {
            $m->url(
                $this->getLinkValueByType($menuItem),
                $menuItem->title,
                null,
                ['target' => $menuItem->target]
            );
        }
    }

    /**
     * Returns the url from menu item
     *
     * @param $menuItem
     * @return string
     */
    protected function getLinkValueByType($menuItem)
    {
        if ($menuItem->is_page) {
            $page = Page::find($menuItem->value);
            return $page ? $page->url : '';
        }

        return $menuItem->value;
    }
}
