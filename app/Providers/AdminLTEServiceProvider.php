<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AdminLTEServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            base_path('vendor/almasaeed2010/adminlte/dist') => public_path('packages/adminlte'),
            base_path('vendor/almasaeed2010/adminlte/plugins') => public_path('packages/adminlte/plugins'),
            base_path('vendor/almasaeed2010/adminlte/bootstrap') => public_path('packages/adminlte/plugins/bootstrap'),
            base_path('vendor/fortawesome/font-awesome/css') => public_path('packages/adminlte/plugins/font-awesome/css'),
            base_path('vendor/fortawesome/font-awesome/fonts') => public_path('packages/adminlte/plugins/font-awesome/fonts'),
        ], 'adminlte');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
