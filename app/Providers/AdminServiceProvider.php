<?php namespace App\Providers;

use App\Models\AdminMenu;
use App\Models\AdminTable;
use Illuminate\Support\ServiceProvider;

class AdminServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (!\Schema::hasTable('translations')) {
            return;
        }

        // Admin Menu
        $adminMenuArray = [];
        foreach (AdminMenu::where('parent_id', '=', '0')->orderBy('position', 'ASC')->get() as $menuItem) {
            $data = [
                'title'             => trans('backend.' . $menuItem->title),
                'icon'              => $menuItem->icon,
                'super_admin_only'  => $menuItem->super_admin_only,
            ];

            $adminMenuArray[$menuItem->route] = [
                'title'             => trans('backend.' . $menuItem->title),
                'icon'              => $menuItem->icon,
                'super_admin_only'  => $menuItem->super_admin_only,
            ];

            $adminSubMenuArray = [];
            foreach (AdminMenu::where('parent_id', '=', $menuItem->id)->orderBy('position', 'ASC')->get() as $subMenuItem) {
                $adminSubMenuArray[$subMenuItem->route] = [
                    'title'             => trans('backend.' . $subMenuItem->title),
                    'super_admin_only'  => $subMenuItem->super_admin_only,
                ];
            }

            if ($adminSubMenuArray) {
                $data['submenu'] = $adminSubMenuArray;
            }

            $adminMenuArray[$menuItem->route] = $data;
        }
        config(['admin.menu' => $adminMenuArray]);


        // Admin Datatable Column
        $adminTableArray = [];
        foreach (AdminTable::all() as $table) {
            if ($table->fields) {
                $data = [];
                foreach (json_decode($table->fields) as $field) {
                    $data[$field->field] = [
                        'title'     => $field->title,
                        'callback'  => function ($value, $item) use ($field) {
                            return eval(str_replace('<?php', '', $field->callback));
                        }
                    ];
                }

                $adminTableArray[$table->controller]['fields'] = $data;
            }
        }
        config(['admin.tables' => $adminTableArray]);


        // Remove menu item for non-super admin
        $adminMenu = config('admin.menu');
        $routeList = app('router')->getRoutes();
        foreach ($adminMenu as $route => $menu) {
            $routeName = $routeList
                ->getByName($route);

            if ($routeName) {
                $middleware = $routeList
                    ->getByName($route)
                    ->middleware();

                if (in_array('admin.superAdminOnly', $middleware)) {
                    $adminMenu[$route]['superAdminOnly'] = true;
                }
            }
        }
        config(['admin.menu' => $adminMenu]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
