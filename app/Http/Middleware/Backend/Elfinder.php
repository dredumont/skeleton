<?php namespace App\Http\Middleware\Backend;

use Closure;
use Illuminate\Contracts\Routing\Middleware;

class Elfinder implements Middleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->get('controller')) {
            \Session::set('elfinder.controller', $request->get('controller'));
        }

        $config = \Config::get('elfinder.dir');
        $path = reset($config);

        $controller = \Session::get('elfinder.controller');

        if (!file_exists($path . '/' . $controller)) {
            mkdir($path . '/' . $controller, 0755);
        }

        if ($request->get('CKEditor')) {
            \Config::set('elfinder.dir', [$path]);
            \Session::set('elfinder.controller', null);
        } else {
            \Config::set('elfinder.dir', [$path . '/' . $controller]);
        }

        return $next($request);
    }
}
