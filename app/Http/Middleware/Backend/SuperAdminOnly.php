<?php namespace App\Http\Middleware\Backend;

use Closure;
use App\Models\AdminMenu;
use Illuminate\Contracts\Auth\Guard;

class SuperAdminOnly
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard $auth
     * @return mixed
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $routeArray = explode('.', $request->route()->getName());
        $routeName = $routeArray[0] . '.' . $routeArray[1];

        $route = AdminMenu::where('route', $routeName)->first();
        if ($route) {
            if ($this->auth->user()->is('admin') && $route->super_admin_only) {
                if ($request->ajax()) {
                    return response('Unauthorized.', 401);
                } else {
                    return redirect()->guest(route('admin'));
                }
            }
        } else {
            if (!$this->auth->user()->is('super.admin')) {
                if ($request->ajax()) {
                    return response('Unauthorized.', 401);
                } else {
                    return redirect()->guest(route('admin'));
                }
            }
        }

        return $next($request);
    }
}
