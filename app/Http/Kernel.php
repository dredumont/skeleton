<?php namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        'Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode',
        'Illuminate\Cookie\Middleware\EncryptCookies',
        'Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse',
        'Illuminate\Session\Middleware\StartSession',
        'Illuminate\View\Middleware\ShareErrorsFromSession',
        'App\Http\Middleware\VerifyCsrfToken',
        'App\Http\Middleware\Locale',
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth'    => 'App\Http\Middleware\Backend\Authenticate',
        'guest'   => 'App\Http\Middleware\Backend\RedirectIfAuthenticated',

        'admin.auth'            => 'App\Http\Middleware\Backend\Authenticate',
        'admin.superAdminOnly'  => 'App\Http\Middleware\Backend\SuperAdminOnly',
        'admin.guest'           => 'App\Http\Middleware\Backend\RedirectIfAuthenticated',
        'admin.elfinder'        => 'App\Http\Middleware\Backend\Elfinder',

        'site.auth'     => 'App\Http\Middleware\Frontend\Authenticate',
        'site.guest'    => 'App\Http\Middleware\Frontend\RedirectIfAuthenticated',

        'localize'              => 'Mcamara\LaravelLocalization\Middleware\LaravelLocalizationRoutes',
        'localizationRedirect'  => 'Mcamara\LaravelLocalization\Middleware\LaravelLocalizationRedirectFilter',
    ];
}
