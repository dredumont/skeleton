<?php namespace App\Http\Controllers\Frontend;

use League\Glide\Http\SignatureFactory;

/**
 * Class FilesController
 * @package App\Http\Controllers\Frontend
 */
class FilesController extends Controller
{
    /**
     * Serving files
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse|void
     */
    public function index()
    {
        if (!\Auth::check() && config('laravel-glide.useSecureURLs') === true) {
            SignatureFactory::create(config('app.key'))->validateRequest(app('request'));
        }

        $path = urldecode(\Request::server('REQUEST_URI'));
        $storagePath    = storage_path() . '/app/data' . str_replace('?' . \Request::server('QUERY_STRING'), '', $path);

        if (!file_exists($storagePath)) {
            return abort(404);
        }

        return response()->download($storagePath);
    }
}