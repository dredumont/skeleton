<?php namespace App\Http\Controllers\Frontend;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

/**
 * Class Controller
 * @package App\Http\Controllers\Frontend
 */
abstract class Controller extends BaseController
{
    use DispatchesCommands, ValidatesRequests;
}
