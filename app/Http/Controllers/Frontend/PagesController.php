<?php namespace App\Http\Controllers\Frontend;

use App\Services\PageService;

/**
 * Class PagesController
 * @package App\Http\Controllers\Frontend
 */
class PagesController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        view()->share('controller', 'page');
    }

    /**
     * @param PageService $pageService
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index(PageService $pageService)
    {
        $page = config('app.currentPage');

        if (!$page->active ||
            ($page->publish_date > date('Y-m-d H:i:s') && $page->publish_date != '0000-00-00 00:00:00') ||
            ($page->expiry_date <= date('Y-m-d H:i:s') && $page->expiry_date != '0000-00-00 00:00:00')
        ) {
            return abort(404);
        }

        $template = $page->template ?: 'index';

        $page->meta_title ? view()->share('pageTitle', $page->meta_title) : view()->share('pageTitle', $page->title);
        $page->meta_description ? view()->share('pageDescription', $page->meta_description) : null;

        return view('frontend.pages.' . $template, [
            'breadcrumb'    => $pageService->getBreadcrumb(),
            'page'          => $page,
        ]);
    }
}
