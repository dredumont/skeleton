<?php namespace App\Http\Controllers\Frontend;

/**
 * Class HomeController
 * @package App\Http\Controllers\Frontend
 */
class HomeController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        view()->share('controller', 'home');
    }

    /**
     * Home view
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index()
    {
        return view('frontend.home.index', [
            'pageTitle' => trans('home.page_title'),
        ]);
    }
}
