<?php namespace App\Http\Controllers\Frontend;

/**
 * Class SitemapController
 * @package App\Http\Controllers\Frontend
 */
class SitemapController extends Controller
{
    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index()
    {
        // ...
    }

    /**
     * @return string
     */
    public function render()
    {
        set_time_limit(0);

        $date = date('Y-m-01');

        $sitemap = app('sitemap');

        foreach (config('laravellocalization.supportedLocales') as $locale => $param) {
            // Home
            $sitemap->add(\LaravelLocalization::getLocalizedURL($locale, '/'), $date, '0.5', 'monthly');
        }

        return $sitemap->render('xml');
    }
}
