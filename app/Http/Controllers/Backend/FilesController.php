<?php namespace App\Http\Controllers\Backend;

use League\Glide\Http\UrlBuilder;
use League\Glide\Http\Signature;

/**
 * Class FilesController
 * @package App\Http\Controllers\Backend
 */
class FilesController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        view()->share('controller', 'files');
        view()->share('title', trans('backend.Files'));

        \Session::set('elfinder.controller', 'files');
        \Session::set('elfinder.category', null);
    }

    /**
     * Files view
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('backend.files.index');
    }

    public function ajax()
    {
        switch (\Request::get('action')) {
            case 'getSignedUrl':
                $urlBuilder = new UrlBuilder(null, new Signature(config('app.key')));

                $output = [];
                foreach (\Request::get('paths') as $key => $value) {
                    $url = str_replace('%2F', '/', urlencode($value['url']));

                    $params = isset($value['params']) ? $value['params'] : [];
                    $output[$key] = url() . $urlBuilder->getUrl($url, $params);
                }

                return $output;

                break;
        }
    }
}
