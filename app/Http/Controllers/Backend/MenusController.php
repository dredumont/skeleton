<?php namespace App\Http\Controllers\Backend;

use App\Models\Menu;
use App\Models\MenuItem;
use App\Http\Requests\Backend\Menus\StoreRequest;
use App\Http\Requests\Backend\Menus\UpdateRequest;
use App\Services\MenuItemService;

/**
 * Class MenusController
 * @package App\Http\Controllers\Backend
 */
class MenusController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        view()->share('controller', 'menus');
        view()->share('title', trans('backend.Menus'));
    }

    /**
     * Menus view
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('backend.default.index', [
            'fieldList' => config('admin.tables')['menus']['fields'],
            'itemList'  => Menu::all(),
        ]);
    }

    /**
     * Create Menus view
     *
     * @param  MenuItemService       $menuItemService
     * @return \Illuminate\View\View
     */
    public function create(MenuItemService $menuItemService)
    {
        $item = new Menu;

        return view('backend.menus.form', [
            'item' => $item,
            'targetList'    => $menuItemService->getTargetList(),
            'localeList'    => $menuItemService->getLocaleList()
        ]);
    }

    /**
     * Store Menus processing
     *
     * @param  StoreRequest                      $request
     * @param  MenuItemService                   $menuItemService
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request, MenuItemService $menuItemService)
    {
        $data = \Input::all();

        Menu::create($data);

        return redirect()->route('admin.menus');
    }

    /**
     * Edit Menus view
     *
     * @param $id
     * @param  MenuItemService       $menuItemService
     * @return \Illuminate\View\View
     */
    public function edit($id, MenuItemService $menuItemService)
    {
        $item = Menu::findOrFail($id);

        return view('backend.menus.form', [
            'item'          => $item,
            'targetList'    => $menuItemService->getTargetList(),
            'localeList'    => $menuItemService->getLocaleList()
        ]);
    }

    /**
     * Update Menus processing
     *
     * @param $id
     * @param  UpdateRequest                     $request
     * @param  MenuItemService                   $menuItemService
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, UpdateRequest $request, MenuItemService $menuItemService)
    {
        $data = \Input::all();

        $item = Menu::findOrFail($id);
        $item->update($data);

        if (isset($data['menu_items'])) {
            foreach ($data['menu_items'] as $menuItemId => $menuItemData) {
                if (is_numeric($menuItemId)) {
                    MenuItem::findOrFail($menuItemId)->update($menuItemData);
                } else {
                    $menuItemData['menu_id'] = $id;
                    $menuItem = MenuItem::create($menuItemData);
                    $data['menu_items'][$menuItem->id] = $menuItemData;

                    foreach (config('laravellocalization.supportedLocales') as $locale => $param) {
                        $data['menu_items_tree_' . $locale] = str_replace($menuItemId, $menuItem->id, $data['menu_items_tree_' . $locale]);
                    }

                    unset($data['menu_items'][$menuItemId]);
                }
            }

            $menuItemService->saveMenuItemTree($data);
        }
        
        $menuItemService->deleteMenuItem($data);

        return redirect()->route('admin.menus');
    }

    /**
     * Delete Menus processing
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function destroy($id)
    {
        $item = Menu::findOrFail($id);
        $item->delete();

        return response()->json(['success' => true]);
    }

    /**
     * Ajax Menus processing
     *
     * @param $id
     * @param  MenuItemService                            $menuItemService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function ajax($id, MenuItemService $menuItemService)
    {
        switch (\Request::input('action')) {
            case 'addMenuItem':
                $menuItem = new MenuItem;
                $menuItem->id = md5(uniqid());
                $menuItem->title = trans('backend.New Item');
                $menuItem->{'is_' . \Request::input('itemType')} = true;

                return view('backend.menus.partials.menuItem')
                    ->with('item', Menu::find($id))
                    ->with('menuItem', $menuItem)
                    ->with('targetList', $menuItemService->getTargetList())
                    ->with('locale', \Request::input('locale'));
                break;
        }

        return response()->json(['success' => true]);
    }
}
