<?php namespace App\Http\Controllers\Backend;

use App\Models\AdminMenu;
use App\Services\AdminMenuService;
use App\Http\Requests\Backend\AdminMenus\StoreRequest;

/**
 * Class AdminMenusController
 * @package App\Http\Controllers\Backend
 */
class AdminMenusController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        view()->share('controller', 'adminMenus');
        view()->share('title', trans('backend.Admin Menus'));
    }

    /**
     * AdminMenus view
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('backend.adminMenus.form', [
            'itemList' => AdminMenu::where('parent_id', '=', '0')->orderBy('position', 'ASC')->get(),
        ]);
    }

    /**
     * Store Menus processing
     *
     * @param  StoreRequest                      $request
     * @param  AdminMenuService                  $adminMenuService
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request, AdminMenuService $adminMenuService)
    {
        $data = \Input::all();

        if (isset($data['menu_items'])) {
            foreach ($data['menu_items'] as $menuItemId => $menuItemData) {
                if (is_numeric($menuItemId)) {
                    AdminMenu::findOrFail($menuItemId)->update($menuItemData);
                } else {
                    $menuItem = AdminMenu::create($menuItemData);
                    $data['menu_items'][$menuItem->id] = $menuItemData;

                    $data['menu_items_tree'] = str_replace($menuItemId, $menuItem->id, $data['menu_items_tree']);

                    unset($data['menu_items'][$menuItemId]);
                }
            }

            $adminMenuService->saveMenuItemTree($data);
        }

        $adminMenuService->deleteMenuItem($data);

        return redirect()->route('admin.adminMenus');
    }

    /**
     * Ajax Menus processing
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function ajax()
    {
        switch (\Request::input('action')) {
            case 'addMenuItem':
                $menuItem = new AdminMenu;
                $menuItem->id = md5(uniqid());
                $menuItem->title = trans('backend.New Item');

                return view('backend.adminMenus.partials.menuItem')
                    ->with('menuItem', $menuItem);
                break;
        }

        return response()->json(['success' => true]);
    }
}
