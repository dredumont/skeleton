<?php namespace App\Http\Controllers\Backend;

use Bican\Roles\Models\Permission;
use App\Http\Requests\Backend\Permissions\StoreRequest;
use App\Http\Requests\Backend\Permissions\UpdateRequest;

/**
 * Class PermissionsController
 * @package App\Http\Controllers\Backend
 */
class PermissionsController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        view()->share('controller', 'permissions');
        view()->share('title', trans('backend.Permissions'));
    }

    /**
     * Permissions view
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('backend.default.index', [
            'fieldList' => config('admin.tables')['permissions']['fields'],
            'itemList'  => Permission::all(),
        ]);
    }

    /**
     * Create Permissions view
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $item = new Permission;

        return view('backend.permissions.form', [
            'item' => $item
        ]);
    }

    /**
     * Store Permissions processing
     *
     * @param  StoreRequest                      $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        Permission::create([
            'name'          => \Input::get('name'),
            'description'   => \Input::get('description'),
        ]);

        return redirect()->route('admin.permissions');
    }

    /**
     * Edit Permissions view
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $item = Permission::findOrFail($id);

        return view('backend.permissions.form', [
            'item' => $item
        ]);
    }

    /**
     * Update Permissions processing
     *
     * @param $id
     * @param  UpdateRequest                     $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, UpdateRequest $request)
    {
        $data = \Input::all();

        $item = Permission::findOrFail($id);
        $item->update($data);

        return redirect()->route('admin.permissions');
    }

    /**
     * Delete Permissions processing
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function destroy($id)
    {
        $item = Permission::findOrFail($id);
        $item->delete();

        return response()->json(['success' => true]);
    }
}
