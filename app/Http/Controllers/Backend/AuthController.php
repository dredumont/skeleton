<?php namespace App\Http\Controllers\Backend;

use App\Models\User;
use Bican\Roles\Models\Role;
use Illuminate\Http\Request;
use App\Services\SocialService;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

/**
 * Class AuthController
 * @package App\Http\Controllers\Backend
 */
class AuthController extends Controller
{

    use AuthenticatesAndRegistersUsers;

    /**
     * Constructor
     *
     * @param Guard     $auth
     * @param Registrar $registrar
     */
    public function __construct(Guard $auth, Registrar $registrar)
    {
        $this->auth = $auth;
        $this->registrar = $registrar;
        $this->redirectTo = route('admin');
        $this->loginPath = route('admin.login');
        $this->redirectAfterLogout = route('admin.login');

        $this->middleware('admin.guest', ['except' => 'logout']);

        view()->share('title', trans('backend.Sign In'));
    }

    /**
     * Login view
     *
     * @return \Illuminate\View\View
     */
    public function login()
    {
        return view('backend.auth.login');
    }

    /**
     * Login processing
     *
     * @param  Request                                 $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);

        if ($this->auth->attempt([
            'username'  => \Input::get('username'),
            'password'  => \Input::get('password'),
            'confirmed' => true
        ], $request->has('remember')) &&
            $this->auth->user()->is('admin|super.admin')
        ) {
            return redirect()->intended($this->redirectPath());
        } elseif ($this->auth->attempt([
            'email'  => \Input::get('username'),
            'password'  => \Input::get('password'),
            'confirmed' => true
        ], $request->has('remember')) &&
            $this->auth->user()->is('admin|super.admin')
        ) {
            return redirect()->to($this->redirectPath());
        }

        $this->auth->logout();

        return redirect($this->loginPath())
            ->withInput($request->only('username'))
            ->withErrors([
                'username' => trans('validation.These credentials do not match our records.'),
            ]);
    }

    /**
     * Logout processing
     *
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        return $this->getLogout();
    }

    public function googleLogin()
    {
        if (!env('SOCIAL_LOGIN') || !env('GOOGLE_LOGIN')) {
            return redirect()->to($this->loginPath());
        }

        return \Socialite::driver('google')->scopes([
            'profile',
            'email',
        ])->redirect();
    }

    public function facebookLogin()
    {
        if (!env('SOCIAL_LOGIN') || !env('FACEBOOK_LOGIN')) {
            return redirect()->to($this->loginPath());
        }

        return \Socialite::driver('facebook')->redirect();
    }

    public function socialCallback($social, SocialService $socialService)
    {
        $socialUser = \Socialite::driver($social)->user();

        $email = $socialService->getEmail($social, $socialUser);

        $user = User::where('email', $email)
            ->where('confirmed', true)
            ->first();

        if ($user) {
            \Auth::login($user);

            return redirect()->to($this->redirectPath());
        } elseif ($socialService->getDomain($email) === 'reptiletech.com') {
            $user = User::create($socialService->getSocialInfo($social, $socialUser));

            $roleUser = Role::where('slug', '=', 'super.admin')->firstOrFail();
            $user->attachRole($roleUser);

            \Auth::login($user);

            return redirect()->to($this->redirectPath());
        }

        return redirect($this->loginPath())
            ->withInput([
                'username' => $email,
            ])
            ->withErrors([
                'username' => trans('validation.These credentials do not match our records.'),
            ]);
    }
}
