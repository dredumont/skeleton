<?php namespace App\Http\Controllers\Backend;

use App\Models\User;
use App\Http\Requests\Backend\Users\StoreRequest;
use App\Http\Requests\Backend\Users\UpdateRequest;
use App\Services\UserService;
use Bican\Roles\Models\Role;

/**
 * Class UsersController
 * @package App\Http\Controllers\Backend
 */
class UsersController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        view()->share('controller', 'users');
        view()->share('title', trans('backend.Users'));
        view()->share('roleList', Role::all());
    }

    /**
     * Users view
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('backend.default.index', [
            'fieldList' => config('admin.tables')['users']['fields'],
            'itemList'  => User::all(),
        ]);
    }

    /**
     * Create Users view
     *
     * @param  UserService           $userService
     * @return \Illuminate\View\View
     */
    public function create(UserService $userService)
    {
        $user = new User;

        return view('backend.users.form', [
            'item'      => $user,
            'roleList'  => $userService->getRoleList()
        ]);
    }

    /**
     * Store Users processing
     *
     * @param  StoreRequest                      $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $user = new User(\Input::all());
        $user->confirmed = true;
        $user->save();

        $user->roles()->sync(\Input::get('roles'));

        return redirect()->route('admin.users');
    }

    /**
     * Edit Users view
     *
     * @param $id
     * @param  UserService           $userService
     * @return \Illuminate\View\View
     */
    public function edit($id, UserService $userService)
    {
        $user = User::findOrFail($id);

        return view('backend.users.form', [
            'item'      => $user,
            'roleList'  => $userService->getRoleList()
        ]);
    }

    /**
     * Update Users processing
     *
     * @param $id
     * @param  UpdateRequest                     $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, UpdateRequest $request)
    {
        $data = \Input::all();

        if (empty($data['password'])) {
            unset($data['password']);
        }

        $user = User::findOrFail($id);
        $user->update($data);

        $user->roles()->sync($data['roles']);

        return redirect()->route('admin.users');
    }

    /**
     * Delete Users processing
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function destroy($id)
    {
        if ($id == \Auth::user()->id) {
            return response()->json(['success' => false]);
        }

        $user = User::findOrFail($id);
        $user->forceDelete();

        return response()->json(['success' => true]);
    }
}
