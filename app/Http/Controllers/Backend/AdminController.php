<?php namespace App\Http\Controllers\Backend;

/**
 * Class AdminController
 * @package App\Http\Controllers\Backend
 */
class AdminController extends Controller
{

    /**
     * Constructor
     */
    public function __construct()
    {
        view()->share('controller', 'admin');
        view()->share('title', trans('backend.Dashboard'));
    }

    /**
     * Admin view
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('backend.admin.index');
    }
}
