<?php namespace App\Http\Controllers\Backend;

use Bican\Roles\Models\Role;
use App\Http\Requests\Backend\Roles\StoreRequest;
use App\Http\Requests\Backend\Roles\UpdateRequest;

/**
 * Class RolesController
 * @package App\Http\Controllers\Backend
 */
class RolesController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        view()->share('controller', 'roles');
        view()->share('title', trans('backend.Roles'));
    }

    /**
     * Roles view
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('backend.default.index', [
            'fieldList' => config('admin.tables')['roles']['fields'],
            'itemList'  => Role::all(),
        ]);
    }

    /**
     * Create Roles view
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $item = new Role;

        return view('backend.roles.form', [
            'item' => $item
        ]);
    }

    /**
     * Store Roles view
     *
     * @param  StoreRequest                      $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        Role::create([
            'name'          => \Input::get('name'),
            'slug'          => \Input::get('name'),
            'description'   => \Input::get('description'),
        ]);

        return redirect()->route('admin.roles');
    }

    /**
     * Edit Roles view
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $item = Role::findOrFail($id);

        return view('backend.roles.form', [
            'item' => $item
        ]);
    }

    /**
     * Update Roles processing
     *
     * @param $id
     * @param  UpdateRequest                     $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, UpdateRequest $request)
    {
        $data = \Input::all();

        $item = Role::findOrFail($id);
        $item->update($data);

        return redirect()->route('admin.roles');
    }

    /**
     * Delete Roles processing
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function destroy($id)
    {
        $item = Role::findOrFail($id);
        $item->delete();

        return response()->json(['success' => true]);
    }
}
