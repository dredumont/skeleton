<?php namespace App\Http\Controllers\Backend;

use App\Models\AdminTable;
use App\Http\Requests\Backend\AdminTables\StoreRequest;
use App\Http\Requests\Backend\AdminTables\UpdateRequest;

/**
 * Class AdminTablesController
 * @package App\Http\Controllers\Backend
 */
class AdminTablesController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        view()->share('controller', 'adminTables');
        view()->share('title', trans('backend.Admin Tables'));
    }

    /**
     * AdminTables view
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('backend.default.index', [
            'fieldList' => config('admin.tables')['adminTables']['fields'],
            'itemList'  => AdminTable::all(),
        ]);
    }

    /**
     * Create AdminTables view
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $item = new AdminTable;

        return view('backend.adminTables.form', [
            'item' => $item
        ]);
    }

    /**
     * Store AdminTables processing
     *
     * @param  StoreRequest                      $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $data = \Input::all();

        AdminTable::create($data);

        return redirect()->route('admin.adminTables');
    }

    /**
     * Edit AdminTables view
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $item = AdminTable::findOrFail($id);

        return view('backend.adminTables.form', [
            'item' => $item
        ]);
    }

    /**
     * Update AdminTables processing
     *
     * @param $id
     * @param  UpdateRequest                     $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, UpdateRequest $request)
    {
        $data = \Input::all();
        $data['fields'] = isset($data['field_items']) ? json_encode($data['field_items']) : '';

        $item = AdminTable::findOrFail($id);
        $item->update($data);

        return redirect()->route('admin.adminTables');
    }

    /**
     * Delete AdminTables processing
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function destroy($id)
    {
        $item = AdminTable::findOrFail($id);
        $item->delete();

        return response()->json(['success' => true]);
    }

    /**
     * Ajax AdminTables processing
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function ajax($id)
    {
        switch (\Request::input('action')) {
            case 'addFieldItem':
                $fieldItem = new \stdClass;
                $fieldItem->id = md5(uniqid());
                $fieldItem->title = trans('backend.New Item');
                $fieldItem->field = '';
                $fieldItem->callback = "<?php\r\nreturn \$value;";

                return view('backend.adminTables.partials.fieldItem')
                    ->with('item', AdminTable::find($id))
                    ->with('fieldItem', $fieldItem);
                break;
        }

        return response()->json(['success' => true]);
    }
}
