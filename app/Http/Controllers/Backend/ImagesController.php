<?php namespace App\Http\Controllers\Backend;

/**
 * Class ImagesController
 * @package App\Http\Controllers\Backend
 */
class ImagesController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        view()->share('controller', 'images');
        view()->share('title', trans('backend.Images'));

        \Session::set('elfinder.controller', 'images');
        \Session::set('elfinder.category', null);
    }

    /**
     * Images view
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('backend.images.index');
    }

    /**
     * Ajax call to clean the /public/images folder of unused images
     * Should be used after heavy image manipulation
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function clearCache()
    {
        $imagesPath = public_path() . '/images';

        \File::cleanDirectory($imagesPath);

        return response()->json(['success' => true]);
    }
}
