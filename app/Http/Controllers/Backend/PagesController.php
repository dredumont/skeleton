<?php namespace App\Http\Controllers\Backend;

use App\Models\Page;
use App\Http\Requests\Backend\Pages\StoreRequest;
use App\Http\Requests\Backend\Pages\UpdateRequest;
use App\Services\PageService;

/**
 * Class PagesController
 * @package App\Http\Controllers\Backend
 */
class PagesController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        view()->share('controller', 'pages');
        view()->share('title', trans('backend.Pages'));
    }

    /**
     * Pages view
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('backend.default.index', [
            'fieldList' => config('admin.tables')['pages']['fields'],
            'itemList'  => Page::all(),
        ]);
    }

    /**
     * Create Pages view
     *
     * @param  PageService           $pageService
     * @return \Illuminate\View\View
     */
    public function create(PageService $pageService)
    {
        $item = new Page;

        return view('backend.pages.form', [
            'item'          => $item,
            'localeList'    => $pageService->getLocaleList(),
            'templateList'  => $pageService->getPageTemplateList()
        ]);
    }

    /**
     * Store Pages processing
     *
     * @param  StoreRequest                      $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $data = \Input::all();

        // Check if route exists
        $url = \Request::root() . '/' . $data['url'];

        $handle = curl_init($url);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($handle);
        $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
        curl_close($handle);

        if ($httpCode != 404) {
            return redirect()->back()
                ->withInput()
                ->withErrors(['email' => trans('validation.Route already exists.')]);
        }

        Page::create($data);

        return redirect()->route('admin.pages');
    }

    /**
     * Edit Pages view
     *
     * @param $id
     * @param  PageService           $pageService
     * @return \Illuminate\View\View
     */
    public function edit($id, PageService $pageService)
    {
        $item = Page::findOrFail($id);

        return view('backend.pages.form', [
            'item'          => $item,
            'localeList'    => $pageService->getLocaleList(),
            'templateList'  => $pageService->getPageTemplateList()
        ]);
    }

    /**
     * Update Pages processing
     *
     * @param $id
     * @param  UpdateRequest                     $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, UpdateRequest $request)
    {
        $data = \Input::all();

        $item = Page::findOrFail($id);
        $item->update($data);

        return redirect()->route('admin.pages');
    }

    /**
     * Delete Pages processing
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function destroy($id)
    {
        $item = Page::findOrFail($id);
        $item->delete();

        return response()->json(['success' => true]);
    }
}
