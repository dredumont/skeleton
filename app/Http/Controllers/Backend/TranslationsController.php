<?php namespace App\Http\Controllers\Backend;

use App\Models\Translation;
use App\Services\TranslationManager;

/**
 * Class TranslationsController
 * @package App\Http\Controllers\Backend
 */
class TranslationsController extends Controller
{
    /**
     * @var TranslationManager
     */
    protected $manager;

    /**
     * Constructor
     *
     * @param TranslationManager $manager
     */
    public function __construct(TranslationManager $manager)
    {
        view()->share('controller', 'translations');
        view()->share('title', trans('backend.Translations'));

        $this->manager = $manager;
    }

    /**
     * Translations view
     *
     * @param  null  $group
     * @return $this
     */
    public function index($group = null)
    {
        $locales = $this->loadLocales();
        $groups = Translation::groupBy('group');
        $excludedGroups = $this->manager->getConfig('exclude_groups');

        if ($excludedGroups) {
            $groups->whereNotIn('group', $excludedGroups);
        }

        $groups = ['' => trans('backend.Choose a group')] + $groups->lists('group', 'group');
        $numChanged = Translation::where('group', $group)
            ->where('status', Translation::STATUS_CHANGED)->count();

        $allTranslations = Translation::where('group', $group)
            ->orderBy('key', 'asc')->get();
        $numTranslations = count($allTranslations);

        $translations = [];
        foreach ($allTranslations as $translation) {
            $translations[$translation->key][$translation->locale] = $translation;
        }

        return view('backend.translations.index')
            ->with('translations', $translations)
            ->with('locales', $locales)
            ->with('groups', $groups)
            ->with('group', $group)
            ->with('numTranslations', $numTranslations)
            ->with('numChanged', $numChanged)
            ->with('editUrl', route('admin.translations.postEdit', [$group]))
            ->with('deleteEnabled', $this->manager->getConfig('delete_enabled'));
    }

    /**
     * Edit Translations view
     *
     * @param $group
     * @return $this
     */
    public function edit($group)
    {
        $locales = $this->loadLocales();
        $groups = Translation::groupBy('group');
        $excludedGroups = $this->manager->getConfig('exclude_groups');

        if ($excludedGroups) {
            $groups->whereNotIn('group', $excludedGroups);
        }

        $groups = ['' => trans('backend.Choose a group')] + $groups->lists('group', 'group');
        $numChanged = Translation::where('group', $group)
            ->where('status', Translation::STATUS_CHANGED)->count();

        $allTranslations = Translation::where('group', $group)
            ->orderBy('key', 'asc')->get();
        $numTranslations = count($allTranslations);

        $translations = [];
        foreach ($allTranslations as $translation) {
            $translations[$translation->key][$translation->locale] = $translation;
        }

        return view('backend.translations.edit')
            ->with('translations', $translations)
            ->with('locales', $locales)
            ->with('groups', $groups)
            ->with('group', $group)
            ->with('numTranslations', $numTranslations)
            ->with('numChanged', $numChanged)
            ->with('editUrl', route('admin.translations.postEdit', [$group]))
            ->with('deleteEnabled', $this->manager->getConfig('delete_enabled'));
    }

    /**
     * Load setted locales
     *
     * @return array
     */
    protected function loadLocales()
    {
        // Set the default locale as the first one.
        $locales = array_merge(
            [\Config::get('app.locale')],
            Translation::groupBy('locale')->lists('locale')
        );

        return array_unique($locales);
    }

    /**
     * Adding keys
     *
     * @param $group
     * @return mixed
     */
    public function postAdd($group)
    {
        $keys = explode("\n", \Input::get('keys'));

        foreach ($keys as $key) {
            $key = trim($key);
            if ($group && $key) {
                $this->manager->missingKey('*', $group, $key);
            }
        }

        return \Redirect::back();
    }

    /**
     * Editing translations
     *
     * @param $group
     * @return array
     */
    public function postEdit($group)
    {
        if (!in_array($group, $this->manager->getConfig('exclude_groups'))) {
            $name = \Input::get('name');
            $value = \Input::get('value');

            list($locale, $key) = explode('|', $name, 2);

            $translation = Translation::firstOrNew([
                'locale' => $locale,
                'group' => $group,
                'key' => $key,
            ]);

            $translation->value = (string) $value ?: null;
            $translation->status = Translation::STATUS_CHANGED;
            $translation->save();

            return ['status' => 'ok'];
        }
    }

    /**
     * Delete keys
     *
     * @param $group
     * @param $key
     * @return array
     */
    public function postDelete($group, $key)
    {
        if (!in_array($group, $this->manager->getConfig('exclude_groups')) &&
            $this->manager->getConfig('delete_enabled')
        ) {
            Translation::where('group', $group)->where('key', $key)->delete();

            return ['status' => 'ok', 'success' => true];
        }

        return ['success' => false];
    }

    /**
     * Import translations from resource files
     *
     * @return mixed
     */
    public function postImport()
    {
        $replace = \Input::get('replace', false);
        $counter = $this->manager->importTranslations($replace);

        return \Response::json(['status' => 'ok', 'counter' => $counter]);
    }

    /**
     * Find translations in templates
     *
     * @return mixed
     */
    public function postFind()
    {
        $path = \Input::get('path', '');
        $numFound = $this->manager->findTranslations(base_path() . '/' . $path);

        return \Response::json(['status' => 'ok', 'counter' => (int) $numFound]);
    }

    /**
     * Publish translations to be visible
     *
     * @param $group
     * @return mixed
     */
    public function postPublish($group)
    {
        $this->manager->exportTranslations($group);

        return \Response::json(['status' => 'ok']);
    }
}
