<?php namespace App\Http\Controllers\Backend;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

/**
 * Class Controller
 * @package App\Http\Controllers\Backend
 */
abstract class Controller extends BaseController
{
    use DispatchesCommands, ValidatesRequests;
}
