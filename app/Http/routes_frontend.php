<?php

Route::group(['namespace' => 'Frontend'], function () {
    // Files
    Route::get('files/{param1?}/{param2?}', ['as' => 'site.files', 'uses' => 'FilesController@index']);

    Route::group(['prefix' => LaravelLocalization::setLocale(), 'middleware' => ['localize', 'localizationRedirect']], function () {
        Route::get('/', ['as' => 'site.home', 'uses' => 'HomeController@index']);
    });

    // Dynamic pages
    if (Schema::hasTable('pages')) {
        $url = Request::path();

        $page = \App\Models\Page::where('url', $url)->first();

        if ($page) {
            Config::set('app.currentPage', $page);
            Route::get($url, ['as' => 'site.pages', 'uses' => 'PagesController@index']);
        }
    }

    // Sitemap
    Route::get('sitemap.xml', ['as' => 'site.sitemap.render', 'uses' => 'SitemapController@render']);
});
