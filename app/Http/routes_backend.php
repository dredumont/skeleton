<?php

Route::group(['prefix' => LaravelLocalization::setLocale(), 'middleware' => ['localize', 'localizationRedirect']], function () {
    Route::group(['prefix' => config('admin.prefix'), 'namespace' => 'Backend'], function () {
        Route::get('login/google', ['as' => 'admin.googleLogin', 'uses' => 'AuthController@googleLogin']);
        Route::get('login/facebook', ['as' => 'admin.facebookLogin', 'uses' => 'AuthController@facebookLogin']);

        Route::get('login/{social}/callback', ['as' => 'admin.socialCallback', 'uses' => 'AuthController@socialCallback'])
            ->where('social', '(google|facebook)');

        Route::get('login', ['as' => 'admin.login', 'uses' => 'AuthController@login']);
        Route::post('login', ['as' => 'admin.postLogin', 'uses' => 'AuthController@postLogin']);
        Route::get('logout', ['as' => 'admin.logout', 'uses' => 'AuthController@logout']);

        Route::controller(
            'password',
            'PasswordController',
            [
                'getEmail'  => 'admin.password.getEmail',
                'postEmail' => 'admin.password.postEmail',
                'getReset'  => 'admin.password.getReset',
                'postReset' => 'admin.password.postReset',
            ]
        );

        Route::group(['middleware' => 'admin.auth'], function () {
            Route::get('/', ['as' => 'admin', 'uses' => 'AdminController@index']);

            // Images
            Route::get('images', ['as' => 'admin.images', 'uses' => 'ImagesController@index']);

            // Files
            Route::get('files', ['as' => 'admin.files', 'uses' => 'FilesController@index']);
            Route::any('files/ajax', ['as' => 'admin.files.ajax', 'uses' => 'FilesController@ajax']);

            // Menus
            restful_controller('menus');
            Route::any('menus/{menu}/ajax', ['as' => 'admin.menus.ajax', 'uses' => 'MenusController@ajax']);

            // Pages
            restful_controller('pages');

            // Super-Admin Only
            Route::group(['middleware' => 'admin.superAdminOnly'], function () {
                // Custom Module
                $moduleRouteFilePath = __DIR__ . '/routes_module.php';
                if (File::exists($moduleRouteFilePath)) {
                    $moduleRoutes = [];
                    include $moduleRouteFilePath;
                    foreach ($moduleRoutes as $module => $route) {
                        restful_controller($route['route']);
                    }
                }

                $restfulControllers = [
                    'users',
                    'roles',
                    'permissions',
                    'adminMenus',
                    'adminTables',
                ];

                foreach ($restfulControllers as $controller) {
                    restful_controller($controller);
                }

                // Translations
                Route::get('translations', [
                    'as' => 'admin.translations',
                    'uses' => 'TranslationsController@index'
                ]);
                Route::get('translations/{group}', [
                    'as' => 'admin.translations.edit',
                    'uses' => 'TranslationsController@edit'
                ]);
                Route::post('translations/add/{group}', [
                    'as' => 'admin.translations.postAdd',
                    'uses' => 'TranslationsController@postAdd'
                ]);
                Route::post('translations/edit/{group}', [
                    'as' => 'admin.translations.postEdit',
                    'uses' => 'TranslationsController@postEdit'
                ]);
                Route::delete('translations/delete/{group}/{key}', [
                    'as' => 'admin.translations.postDelete',
                    'uses' => 'TranslationsController@postDelete'
                ]);
                Route::post('translations/import', [
                    'as' => 'admin.translations.postImport',
                    'uses' => 'TranslationsController@postImport'
                ]);
                Route::post('translations/find', [
                    'as' => 'admin.translations.postFind',
                    'uses' => 'TranslationsController@postFind'
                ]);
                Route::post('translations/publish/{group}', [
                    'as' => 'admin.translations.postPublish',
                    'uses' => 'TranslationsController@postPublish'
                ]);

                // Admin Menus
                Route::any('adminMenus/ajax', [
                    'as' => 'admin.adminMenus.ajax',
                    'uses' => 'AdminMenusController@ajax'
                ]);

                // Admin Tables
                Route::any('adminTables/{table}/ajax', [
                    'as' => 'admin.adminTables.ajax',
                    'uses' => 'AdminTablesController@ajax'
                ]);
            });
        });
    });
});
