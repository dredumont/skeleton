<?php namespace App\Http\Requests\Backend\Users;

use App\Http\Requests\Backend\Request;

class UpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $userId = $this->route()->getParameter('users');

        return [
            'firstname' => 'required',
            'lastname'  => 'required',
            'email'     => 'required|email|unique:users,email,' . $userId,
            'username'  => 'required|unique:users,username,' . $userId,
            'password'  => 'confirmed|min:6',
            'roles'     => 'required',
        ];
    }
}
