<?php namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\ModuleMakeCommand',
        'App\Console\Commands\Module\MigrationMakeCommand',
        'App\Console\Commands\Module\RequestMakeCommand',
        'App\Console\Commands\Module\ModelMakeCommand',
        'App\Console\Commands\Module\ViewMakeCommand',
        'App\Console\Commands\Module\ControllerMakeCommand',
        'App\Console\Commands\Module\RouteMakeCommand',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //
    }
}
