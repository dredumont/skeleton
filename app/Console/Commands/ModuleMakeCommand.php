<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Composer;
use Symfony\Component\Console\Input\InputArgument;

class ModuleMakeCommand extends Command
{
    protected $name = 'make:module';
    protected $description = 'Create a new module';
    protected $composer;

    public function __construct(Composer $composer)
    {
        parent::__construct();

        $this->composer = $composer;
    }

    public function fire()
    {
        $module = $this->argument('module');
        
        $this->call('make:module-migration', ['module' => $module]);
        $this->call('make:module-request', ['module' => $module]);
        $this->call('make:module-model', ['module' => $module]);
        $this->call('make:module-view', ['module' => $module]);
        $this->call('make:module-controller', ['module' => $module]);
        $this->call('make:module-route', ['module' => $module]);

        $this->composer->dumpAutoloads();

        $this->info("");
        $this->info("Enjoy your new $module module !");
    }

    protected function getArguments()
    {
        return [
            ['module', InputArgument::REQUIRED, 'The module name.'],
        ];
    }

    protected function getOptions()
    {
        return [];
    }
}
