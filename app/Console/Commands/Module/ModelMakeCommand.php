<?php namespace App\Console\Commands\Module;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Input\InputArgument;

class ModelMakeCommand extends Command
{
    protected $name = 'make:module-model';
    protected $description = 'Create a new module model';
    protected $files;
    protected $type;

    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
    }

    public function fire()
    {
        $module = $this->argument('module');
        $searchPath = $this->getPath() . '/' . $this->getModelFileName($module);
        $fileExists = $this->files->glob($searchPath);

        if ($fileExists === false) {
            die('An error occurred.');
        }

        if ($fileExists) {
            $this->error($module . ' model already exists!');
            return false;
        }

        $this->files->put(
            $this->getPath() . '/' . $this->getModelFileName($module),
            $this->buildStub($module)
        );

        $this->info($module . ' model created successfully.');
    }

    protected function getArguments()
    {
        return [
            ['module', InputArgument::REQUIRED, 'The module name.'],
        ];
    }

    protected function getStub()
    {
        return __DIR__ . '/stubs/model.stub';
    }

    protected function getPath()
    {
        return $this->laravel->path() . '/Models';
    }

    protected function getTableName($module)
    {
        return str_plural(strtolower(preg_replace('/(?<=\\w)(?=[A-Z])/', '_$1', $module)));
    }

    protected function getModelFileName($module)
    {
        return $module . '.php';
    }

    protected function buildStub($module)
    {
        $stub = $this->files->get($this->getStub());

        $stub = str_replace(
            '{{modelName}}',
            $module,
            $stub
        );

        $stub = str_replace(
            '{{tableName}}',
            $this->getTableName($module),
            $stub
        );

        return $stub;
    }
}
