<?php namespace App\Console\Commands\Module;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Input\InputArgument;

class ViewMakeCommand extends Command
{
    protected $name = 'make:module-view';
    protected $description = 'Create a new module view';
    protected $files;
    protected $type;

    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
    }

    public function fire()
    {
        $module = $this->argument('module');
        $searchPath = $this->getPath($module) . '/' . $this->getViewFileName();
        $fileExists = $this->files->glob($searchPath);

        if ($fileExists === false) {
            die('An error occurred.');
        }

        if ($fileExists) {
            $this->error($module . ' view already exists!');
            return false;
        }

        if (!$this->files->isDirectory($this->getPath($module))) {
            $this->files->makeDirectory($this->getPath($module), 0775, true, true);
        }

        $this->files->put(
            $this->getPath($module) . '/' . $this->getViewFileName(),
            $this->buildStub()
        );

        $this->info($module . ' view created successfully.');
    }

    protected function getArguments()
    {
        return [
            ['module', InputArgument::REQUIRED, 'The module name.'],
        ];
    }

    protected function getStub()
    {
        return __DIR__ . '/stubs/view.stub';
    }

    protected function getPath($module)
    {
        return $this->laravel->basePath() . '/resources/views/backend/' . $this->getViewName($module);
    }

    protected function getViewName($module)
    {
        return lcfirst($this->getControllerName($module));
    }

    protected function getControllerName($module)
    {
        return str_plural($module);
    }

    protected function getViewFileName()
    {
        return 'form.blade.php';
    }

    protected function buildStub()
    {
        $stub = $this->files->get($this->getStub());

        return $stub;
    }
}
