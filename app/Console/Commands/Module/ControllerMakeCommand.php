<?php namespace App\Console\Commands\Module;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Input\InputArgument;

class ControllerMakeCommand extends Command
{
    protected $name = 'make:module-controller';
    protected $description = 'Create a new module controller';
    protected $files;
    protected $type;

    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
    }

    public function fire()
    {
        $module = $this->argument('module');
        $searchPath = $this->getPath() . '/' . $this->getControllerFileName($module);
        $fileExists = $this->files->glob($searchPath);

        if ($fileExists === false) {
            die('An error occurred.');
        }

        if ($fileExists) {
            $this->error($module . ' controller already exists!');
            return false;
        }

        $this->files->put(
            $this->getPath() . '/' . $this->getControllerFileName($module),
            $this->buildStub($module)
        );

        $this->info($module . ' controller created successfully.');
    }

    protected function getArguments()
    {
        return [
            ['module', InputArgument::REQUIRED, 'The module name.'],
        ];
    }

    protected function getStub()
    {
        return __DIR__ . '/stubs/controller.stub';
    }

    protected function getPath()
    {
        return $this->laravel->path() . '/Http/Controllers/Backend';
    }

    protected function getViewName($module)
    {
        return lcfirst($this->getControllerName($module));
    }

    protected function getControllerName($module)
    {
        return str_plural($module);
    }

    protected function getControllerPrettyName($module)
    {
        return str_plural(preg_replace('/(?<=\\w)(?=[A-Z])/', ' $1', $module));
    }

    protected function getControllerFileName($module)
    {
        return $this->getControllerName($module) . 'Controller.php';
    }

    protected function buildStub($module)
    {
        $stub = $this->files->get($this->getStub());

        $stub = str_replace(
            '{{controllerName}}',
            $this->getControllerName($module),
            $stub
        );

        $stub = str_replace(
            '{{controllerPrettyName}}',
            $this->getControllerPrettyName($module),
            $stub
        );

        $stub = str_replace(
            '{{modelName}}',
            $module,
            $stub
        );

        $stub = str_replace(
            '{{viewName}}',
            $this->getViewName($module),
            $stub
        );

        return $stub;
    }
}
