<?php namespace App\Console\Commands\Module;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Input\InputArgument;

class MigrationMakeCommand extends Command
{
    protected $name = 'make:module-migration';
    protected $description = 'Create a new module migration';
    protected $files;
    protected $type;

    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
    }

    public function fire()
    {
        $module = $this->argument('module');
        $searchPath = $this->getPath() . '/*' . $this->getMigrationFileName($module);
        $fileExists = $this->files->glob($searchPath);

        if ($fileExists === false) {
            die('An error occurred.');
        }

        if ($fileExists) {
            $this->error($module . ' migration already exists!');
            return false;
        }

        $this->files->put(
            $this->getPath() . '/' . date('Y_m_d_His') . $this->getMigrationFileName($module),
            $this->buildStub($module)
        );

        $this->info($module . ' migration created successfully.');
    }

    protected function getArguments()
    {
        return [
            ['module', InputArgument::REQUIRED, 'The module name.'],
        ];
    }

    protected function getStub()
    {
        return __DIR__ . '/stubs/migration.stub';
    }

    protected function getPath()
    {
        return $this->laravel->databasePath() . '/migrations';
    }

    protected function getMigrationName($module)
    {
        return str_plural($module);
    }

    protected function getMigrationFileName($module)
    {
        return '_create_' . $this->getTableName($module) . '_table.php';
    }

    protected function getTableName($module)
    {
        return str_plural(strtolower(preg_replace('/(?<=\\w)(?=[A-Z])/', '_$1', $module)));
    }

    protected function buildStub($module)
    {
        $stub = $this->files->get($this->getStub());

        $stub = str_replace(
            '{{migrationName}}',
            $this->getMigrationName($module),
            $stub
        );

        $stub = str_replace(
            '{{tableName}}',
            $this->getTableName($module),
            $stub
        );

        return $stub;
    }
}
