<?php namespace App\Http\Controllers\Backend;

use App\Models\{{modelName}};
use App\Http\Requests\Backend\{{controllerName}}\StoreRequest;
use App\Http\Requests\Backend\{{controllerName}}\UpdateRequest;

/**
 * Class {{controllerName}}Controller
 * @package App\Http\Controllers\Backend
 */
class {{controllerName}}Controller extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        view()->share('controller', '{{viewName}}');
        view()->share('title', trans('backend.{{controllerPrettyName}}'));
    }

    /**
     * {{controllerName}} view
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('backend.default.index', [
            'fieldList' => config('admin.tables')['{{viewName}}']['fields'],
            'itemList'  => {{modelName}}::all(),
        ]);
    }

    /**
     * Create {{controllerName}} view
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $item = new {{modelName}};

        return view('backend.{{viewName}}.form', [
            'item' => $item
        ]);
    }

    /**
     * Store {{controllerName}} processing
     *
     * @param  StoreRequest                      $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $data = \Input::all();

        {{modelName}}::create($data);

        return redirect()->route('admin.{{viewName}}');
    }

    /**
     * Edit {{controllerName}} view
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $item = {{modelName}}::findOrFail($id);

        return view('backend.{{viewName}}.form', [
            'item' => $item
        ]);
    }

    /**
     * Update {{controllerName}} processing
     *
     * @param $id
     * @param  UpdateRequest                     $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, UpdateRequest $request)
    {
        $data = \Input::all();

        $item = {{modelName}}::findOrFail($id);
        $item->update($data);

        return redirect()->route('admin.{{viewName}}');
    }

    /**
     * Delete {{controllerName}} processing
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function destroy($id)
    {
        $item = {{modelName}}::findOrFail($id);
        $item->delete();

        return response()->json(['success' => true]);
    }
}
