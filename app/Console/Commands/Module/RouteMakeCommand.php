<?php namespace App\Console\Commands\Module;

use App\Models\AdminMenu;
use App\Models\AdminTable;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Input\InputArgument;

class RouteMakeCommand extends Command
{
    protected $name = 'make:module-route';
    protected $description = 'Create a new module route';
    protected $files;
    protected $type;

    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
    }

    public function fire()
    {
        $module = $this->argument('module');
        $routeFilePath = $this->getPath() . '/' . $this->getRouteFileName();
        $moduleRoutes = [];

        if ($this->files->exists($routeFilePath)) {
            include $routeFilePath;
            if (isset($moduleRoutes[$module])) {
                $this->error($module . ' route already exists!');
                return false;
            }
        }

        $this->files->put($routeFilePath, $this->buildStub($module, $moduleRoutes));

        AdminMenu::create([
            'active'    => true,
            'title'     => $this->getControllerName($module),
            'route'     => 'admin.' . $this->getViewName($module),
            'icon'      => 'fa-circle-o',
        ]);

        AdminTable::create([
            'title'         => $this->getControllerPrettyName($module),
            'controller'    => $this->getViewName($module),
            'fields'        => '{
                "0":{
                    "id":"' . md5(uniqid()) . '",
                    "title":"Title",
                    "field":"title",
                    "callback":"<?php\r\nreturn $value;"
                },
                "1":{
                    "id":"' . md5(uniqid()) . '",
                    "title":"Active",
                    "field":"active",
                    "callback":"<?php\r\nreturn $value ? trans(\'backend.Yes\') : trans(\'backend.No\');"
                }
            }',
        ]);

        $this->info($module . ' route created successfully.');
    }

    protected function getArguments()
    {
        return [
            ['module', InputArgument::REQUIRED, 'The module name.'],
        ];
    }

    protected function getPath()
    {
        return $this->laravel->path() . '/Http';
    }

    protected function getViewName($module)
    {
        return lcfirst($this->getControllerName($module));
    }

    protected function getControllerName($module)
    {
        return str_plural($module);
    }

    protected function getControllerPrettyName($module)
    {
        return str_plural(preg_replace('/(?<=\\w)(?=[A-Z])/', ' $1', $module));
    }

    protected function getRouteFileName()
    {
        return 'routes_module.php';
    }

    protected function buildStub($module, $moduleRoutes)
    {
        $moduleRoutes[$module] = [
            'route' => $this->getViewName($module),
            'as' => 'admin.' . $this->getViewName($module),
            'uses' => $this->getControllerName($module) . 'Controller@index',
            'controller' => $this->getControllerName($module) . 'Controller',
            'attributes' => [
                'except' => [
                    'index',
                    'show'
                ]
            ],
        ];

        $routeModuleOutput = "<?php\n\n\$moduleRoutes = " . var_export($moduleRoutes, true) . ";\n";

        return $routeModuleOutput;
    }
}
