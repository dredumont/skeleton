<?php namespace App\Console\Commands\Module;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Input\InputArgument;

class RequestMakeCommand extends Command
{
    protected $name = 'make:module-request';
    protected $description = 'Create a new module request';
    protected $files;
    protected $type;

    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
    }

    public function fire()
    {
        $module = $this->argument('module');

        foreach (['store', 'update'] as $request) {
            $searchPath = $this->getPath($module) . '/' . $this->getRequestFileName($request);
            $fileExists = $this->files->glob($searchPath);

            if ($fileExists === false) {
                die('An error occurred.');
            }

            if ($fileExists) {
                $this->error($module . ' ' . $request . ' request already exists!');
                return false;
            }

            if (!$this->files->isDirectory($this->getPath($module))) {
                $this->files->makeDirectory($this->getPath($module), 0775, true, true);
            }

            $this->files->put(
                $this->getPath($module) . '/' . $this->getRequestFileName($request),
                $this->buildStub($module, $request)
            );

            $this->info($module . ' ' . $request . ' request created successfully.');
        }
    }

    protected function getArguments()
    {
        return [
            ['module', InputArgument::REQUIRED, 'The module name.'],
        ];
    }

    protected function getStub($request)
    {
        return __DIR__ . '/stubs/' . $request . 'Request.stub';
    }

    protected function getPath($module)
    {
        return $this->laravel->path() . '/Http/Requests/Backend/' . $this->getControllerName($module);
    }

    protected function getControllerName($module)
    {
        return str_plural($module);
    }

    protected function getRequestFileName($request)
    {
        return ucfirst($request) . 'Request.php';
    }

    protected function buildStub($module, $request)
    {
        $stub = $this->files->get($this->getStub($request));

        $stub = str_replace(
            '{{controllerName}}',
            $this->getControllerName($module),
            $stub
        );

        return $stub;
    }
}
