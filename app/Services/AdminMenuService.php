<?php namespace App\Services;

use App\Models\AdminMenu;
use Illuminate\Foundation\Application;

/**
 * Class AdminMenuService
 * @package App\Services
 */
class AdminMenuService
{
    /** @var \Illuminate\Foundation\Application  */
    protected $app;

    /**
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Save menu items order
     *
     * @param $data
     * @return void
     */
    public function saveMenuItemTree($data)
    {
        if ($data['menu_items_tree']) {
            $menuItemsTree = [];
            parse_str($data['menu_items_tree'], $output);
            foreach ($output['menu-item'] as $menuItemId => $parentMenuItemId) {
                $menuItemsTree[$parentMenuItemId][] = $menuItemId;
            }

            foreach ($menuItemsTree as $parentMenuItemId => $menuItem) {
                foreach ($menuItem as $position => $menuItemId) {
                    if (is_numeric($menuItemId)) {
                        AdminMenu::find($menuItemId)->update([
                            'position'  => ++$position,
                            'parent_id' => $parentMenuItemId
                        ]);
                    }
                }
            }
        }
    }

    /**
     * Remove deleted menu items
     *
     * @param $data
     * @return void
     */
    public function deleteMenuItem($data)
    {
        $menuItemIdList = explode(',', trim($data['menu_items_delete'], ','));

        foreach ($menuItemIdList as $menuItemId) {
            AdminMenu::destroy($menuItemId);
        }
    }
}
