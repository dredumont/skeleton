<?php namespace App\Services;

use Illuminate\Foundation\Application;

/**
 * Class SocialService
 * @package App\Services
 */
class SocialService
{
    /** @var \Illuminate\Foundation\Application  */
    protected $app;

    /**
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function getEmail($social, $socialUser)
    {
        switch ($social) {
            case 'google':
            case 'facebook':
                return $socialUser->email;
                break;
        }
    }

    public function getDomain($email)
    {
        $domain = explode('@', $email);
        return end($domain);
    }

    public function getSocialInfo($social, $socialUser)
    {
        switch ($social) {
            case 'google':
                return [
                    'firstname'     => $socialUser->user['name']['givenName'],
                    'lastname'      => $socialUser->user['name']['familyName'],
                    'email'         => $socialUser->email,
                    'username'      => $socialUser->email,
                    'confirmed'     => true,
                ];
                break;

            case 'facebook':
                return [
                    'firstname'     => $socialUser->user['first_name'],
                    'lastname'      => $socialUser->user['last_name'],
                    'email'         => $socialUser->email,
                    'username'      => $socialUser->email,
                    'confirmed'     => true,
                ];
                break;
        }
    }
}
