<?php namespace App\Services;

use Illuminate\Foundation\Application;
use App\Models\Page;

/**
 * Class PageService
 * @package App\Services
 */
class PageService
{
    /** @var \Illuminate\Foundation\Application  */
    protected $app;

    /**
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function getPageTemplateList()
    {
        $templateList = [];

        foreach (\File::files(config('view.paths')[0] . '/frontend/pages') as $path) {
            $f = fopen($path, 'r');
            $line = fgets($f);
            fclose($f);

            preg_match('/{{--(.*)--}}/', $line, $matches);

            $baseName = basename($path);
            $templateArray = explode('.', $baseName);
            $templateFile = reset($templateArray);
            $templateName = trim(end($matches));

            $templateList[$templateFile] = $templateName;
        }

        ksort($templateList);

        return $templateList;
    }

    public function getLocaleList()
    {
        $localeList = ['' => trans('backend.None')];

        foreach (config('laravellocalization.supportedLocales') as $locale => $param) {
            $localeList[$locale] = $param['name'];
        }

        return $localeList;
    }

    public function getBreadcrumb()
    {
        $currentPage = config('app.currentPage');
        $output = [
            'list'      => [],
            'current'   => [
                'title' => $currentPage->title,
            ]
        ];

        $parentId = $currentPage->parent_id;
        while ($parentId > 0) {
            $parent = Page::find($parentId);
            $parentId = $parent->parent_id;
            $output['list'][] = [
                'url'   => $parent->url,
                'title' => $parent->title,
            ];
        }

        return $output;
    }
}
