<?php namespace App\Services;

use Bican\Roles\Models\Role;
use Illuminate\Foundation\Application;

/**
 * Class UserService
 * @package App\Services
 */
class UserService
{
    /** @var \Illuminate\Foundation\Application  */
    protected $app;

    /**
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function getRoleList()
    {
        $roleList = [];

        foreach (Role::all() as $role) {
            $roleList[$role->id] = $role->name;
        }

        return $roleList;
    }
}
