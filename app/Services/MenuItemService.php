<?php namespace App\Services;

use App\Models\MenuItem;
use Illuminate\Foundation\Application;

/**
 * Class MenuItemService
 * @package App\Services
 */
class MenuItemService
{
    /** @var \Illuminate\Foundation\Application  */
    protected $app;

    /**
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Return the link target list
     */
    public function getTargetList()
    {
        return [
            '_self'     => trans('backend._self'),
            '_blank'    => trans('backend._blank'),
            '_parent'   => trans('backend._parent'),
            '_top'      => trans('backend._top'),
        ];
    }

    /**
     * Return the link locale list
     */
    public function getLocaleList()
    {
        foreach (config('laravellocalization.supportedLocales') as $locale => $param) {
            $localeList[$locale] = $param['name'];
        }

        return $localeList;
    }

    /**
     * Save menu items order
     *
     * @param $data
     * @return void
     */
    public function saveMenuItemTree($data)
    {
        foreach (config('laravellocalization.supportedLocales') as $locale => $param) {
            if ($data['menu_items_tree_' . $locale]) {
                $menuItemsTree = [];
                parse_str($data['menu_items_tree_' . $locale], $output);
                foreach ($output['menu-item'] as $menuItemId => $parentMenuItemId) {
                    $menuItemsTree[$parentMenuItemId][] = $menuItemId;
                }

                foreach ($menuItemsTree as $parentMenuItemId => $menuItem) {
                    foreach ($menuItem as $position => $menuItemId) {
                        if (is_numeric($menuItemId)) {
                            MenuItem::find($menuItemId)->update([
                                'position'  => ++$position,
                                'parent_id' => $parentMenuItemId
                            ]);
                        }
                    }
                }
            }
        }
    }

    /**
     * Remove deleted menu items
     *
     * @param $data
     * @return void
     */
    public function deleteMenuItem($data)
    {
        $menuItemIdList = explode(',', trim($data['menu_items_delete'], ','));

        foreach ($menuItemIdList as $menuItemId) {
            MenuItem::destroy($menuItemId);
        }
    }
}
