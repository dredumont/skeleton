<?php namespace App\Services;

use Illuminate\Foundation\Application;

/**
 * Class AppService
 * @package App\Services
 */
class AppService
{
    /** @var \Illuminate\Foundation\Application  */
    protected $app;

    /**
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function getSwitchLang()
    {
        $locale = config('app.locale') == 'fr' ? 'en' : 'fr';

        return \LaravelLocalization::getLocalizedURL($locale);
    }
}
