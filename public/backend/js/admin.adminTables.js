(function($) {

    $(document).on('click', '.btn-add-item', function() {
        if ($.active) {
            return;
        }

        var $btn = $(this);
        $.ajax({
            type: 'POST',
            url: $btn.data('ajax-action'),
            data: {
                action: 'addFieldItem'
            },
            success: function(data) {
                $btn.closest('form').find('.field-items').append(data);

                initAce();
            }
        });
    });

    $(document).on('click', '.btn-delete-item', function() {
        $(this).closest('.panel').remove();
    });

    $('.field-items').sortable({
        handle: '.handle',
        items: 'li',
        listType: 'ul'
    });

    initAce();

})(jQuery);

function initAce() {
    $('.code').ace({
        height: '105',
        lang: 'php',
        width: '100%'
    });
}