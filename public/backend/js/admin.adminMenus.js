(function($) {

    $(document).on('click', '.btn-add-item', function() {
        if ($.active) {
            return;
        }

        var $btn = $(this);
        $.ajax({
            type: 'POST',
            url: $btn.data('ajax-action'),
            data: {
                action: 'addMenuItem'
            },
            success: function(data) {
                $btn.closest('form').find('.menu-items').append(data);
            }
        });
    });

    $(document).on('click', '.btn-delete-item', function() {
        if ($.isNumeric($(this).data('menu-item-id'))) {
            $('input[name="menu_items_delete').val($(this).data('menu-item-id') + ',' + $('input[name="menu_items_delete').val());
        }

        $(this).closest('.panel').remove();
    });

    $('.menu-items').nestedSortable({
        handle: '.handle',
        items: 'li',
        listType: 'ul',
        maxLevels: 5,
        stop: function() {
            $(this).prev('input[type="hidden"]').val($(this).nestedSortable('serialize'));
        }
    });

})(jQuery);