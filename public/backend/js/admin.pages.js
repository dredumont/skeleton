(function($) {

    if ($().slugify && $('#slug').length) {
        $('#slug').slugify();
    }

    $(document).on('change', '#parent_id', function() {
        if ($(this).val()) {
            $('#url').val($(this)
                .find('option:selected')
                .data('url') + '/' + $('#slug').val());
            $('#locale')
                .attr('disabled', 'disabled')
                .val($(this).find('option:selected').data('locale'))
                .change();
            $('#hidden_locale').val($(this)
                .find('option:selected')
                .data('locale'));
        } else {
            $('#url').val($('#slug').val());
            $('#locale')
                .removeAttr('disabled')
                .val('')
                .change();
            $('#hidden_locale').val('');
        }
    });

    $(document).on('change', '#locale', function() {
        if (!$('#parent_id').val() && $(this).val()) {
            $('#url').val($(this).val() + '/' + $('#slug').val());
        } else if(!$('#parent_id').val()) {
            $('#url').val($('#slug').val());
        }
    });

    if (!$('input[name="_method"][value="PUT"]').length) {
        $(document).on('keyup', '#title', function() {
            var url = '';

            if ($('#parent_id').val()) {
                url += $('#parent_id')
                    .find('option:selected')
                    .data('url') + '/';
            }

            if (!$('#parent_id').val() && $('#locale').val()) {
                url += $('#locale').val() + '/';
            }

            url += convertToSlug($('#title').val());
            $('#url').val(url);
            $('#slug').val(convertToSlug($('#title').val()));
        });
    }

    $(document).on('blur', '#slug', function() {
        var url = '';

        if ($('#parent_id').val()) {
            url += $('#parent_id')
                .find('option:selected')
                .data('url') + '/';
        }

        if (!$('#parent_id').val() && $('#locale').val()) {
            url += $('#locale').val() + '/';
        }

        url += convertToSlug($(this).val());
        $('#url').val(url);
    });

    $(document).on('blur', '#ident', function() {
        $(this).val(convertToIdent($(this).val()));
    });

    $(document).on('keyup', '#group_ident', function(){
        $('#ident').val('').change();
    });

    $(document).on('change', '#group_ident', function(){
        if ($(this).val() !== '') {
            $('#ident').val($(this).val());
            $('#ident').prop('readonly', true);
        } else {
            $('#ident').val('');
            $('#ident').prop('readonly', false);
        }
    });
    
})(jQuery);