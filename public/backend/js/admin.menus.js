(function($) {

    $(document).on('blur', '#ident', function() {
        $(this).val(convertToIdent($(this).val()));
    });

    $(document).on('click', '.btn-add-item', function() {
        if ($.active) {
            return;
        }

        var $btn = $(this);
        $.ajax({
            type: 'POST',
            url: $btn.data('ajax-action'),
            data: {
                action: 'addMenuItem',
                itemType: $btn.data('item-type'),
                locale: $btn.data('locale')
            },
            success: function(data) {
                $btn.closest('.tab-pane').find('.menu-items').append(data);
            }
        });
    });

    $(document).on('click', '.btn-delete-item', function() {
        if ($.isNumeric($(this).data('menu-item-id'))) {
            $('input[name="menu_items_delete').val($(this).data('menu-item-id') + ',' + $('input[name="menu_items_delete').val());
        }

        $(this).closest('.panel').remove();
    });

    $('.menu-items').nestedSortable({
        handle: '.handle',
        items: 'li',
        listType: 'ul',
        maxLevels: 5,
        stop: function() {
            $(this).prev('input[type="hidden"]').val($(this).nestedSortable('serialize'));
        }
    });

    $(document).on('click', 'input[type="submit"]', function(e) {
        e.preventDefault();

        $('.tab-pane').each(function() {
            $('input[name="menu_items_tree_' + $(this).attr('id') + '"]').val($('#menu-items-' + $(this).attr('id') + ' .menu-items').nestedSortable('serialize'));
        });
        
        $(this).closest('form').submit();
    });
    
})(jQuery);