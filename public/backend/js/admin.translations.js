(function($) {

    $('.editable').editable().on('hidden', function(e, reason) {
        var locale = $(this).data('locale');
        if (reason === 'save') {
            $(this).removeClass('status-0').addClass('status-1');
        }
    });

    $('#table').on('draw.dt', function() {
        $('#table').find('.editable').not('.editable-click').editable().on('hidden', function(e, reason) {
            var locale = $(this).data('locale');
            if (reason === 'save') {
                $(this).removeClass('status-0').addClass('status-1');
            }
        });
    });

    $(document).on('click', '.editable', function() {
        $(this).editable('show');
    });

    $('.group-select').on('change', function() {
        var group = $(this).val();
        if (group) {
            window.location.href = moduleUrl + '/' + $(this).val();
        } else {
            window.location.href = moduleUrl;
        }
    });

    $('.form-import').on('ajax:success', function(e, data) {
        $('div.success-import strong.counter').text(data.counter);
        $('div.success-import').slideDown();
    });

    $('.form-find').on('ajax:success', function(e, data) {
        $('div.success-find strong.counter').text(data.counter);
        $('div.success-find').slideDown();
    });

    $('.form-publish').on('ajax:success', function(e, data) {
        $('div.success-publish').slideDown();
    });
    
})(jQuery);