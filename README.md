#** Laravel Skeleton **#

[![Latest Stable Version](https://poser.pugx.org/dredumont/skeleton/v/stable)](https://packagist.org/packages/dredumont/skeleton) [![Total Downloads](https://poser.pugx.org/dredumont/skeleton/downloads)](https://packagist.org/packages/dredumont/skeleton) [![Latest Unstable Version](https://poser.pugx.org/dredumont/skeleton/v/unstable)](https://packagist.org/packages/dredumont/skeleton) [![License](https://poser.pugx.org/dredumont/skeleton/license)](https://packagist.org/packages/dredumont/skeleton)

## **Installation** ##

Veuillez au préalable avoir la Vagrant Homestead de Laravel installé sur votre ordinateur.

* Connectez-vous en SSH à votre Vagrant

* Ensuite, naviguez jusque dans votre workspace et entrez la commandes suivantes :


```
#!sh

composer create-project dredumont/skeleton
mv skeleton NOM_DU_PROJET
cd NOM_DU_PROJET
```

* Ouvrir .env et modifier les infos relatives à l'environnement. Vous pouvez modifier le nom de la base de donnée à partir de ce fichier. Les accès n'ont pas besoin d'être changés.

* Créer la base de donnée sur la Vagrant

* Ensuite, toujours à la racine, entrez les commandes suivantes :

```
#!sh

php artisan migrate
php artisan db:seed
php artisan elfinder:publish
php artisan vendor:publish --tag=adminlte
php artisan vendor:publish --provider="Pingpong\Menus\MenusServiceProvider"
```

* Pour ajouter le host sur la Vagrant, entrez : 
  serve NOM_DU_PROJET.app /home/vagrant/PATH_DU_WORKSPACE/PATH_DU_PROJET/public

* Vous pouvez ensuite ajouter la ligne suivante dans votre host pour activer le site : 
  192.168.10.10 NOM_DU_PROJET.app
  
* Vous êtes maintenant en mesure de voir le site. Vous pouvez accéder au backend ici :
  http://NOM_DU_PROJET.app/admin -> Connectez-vous avez votre compte Google Reptiletech

  
=============================================

# **Intégration** #

* Si vous êtes sur Windows, veuillez faire le fix suivant :

```
#!text

# Lines 495-510 - C:\HashiCorp\Vagrant\embedded\gems\gems\vagrant-1.7.2\plugins\providers\virtualbox\driver\version_4_3.rb
- folder[:hostpath]]
+ '\\\\?\\' + folder[:hostpath].gsub(/[\/\\]/,'\\')]
```

  
* Ouvrir le terminal ou Git Bash pour Windows

* Aller à la racine du projet

* Entrez la commande suivante : 

```
#!sh

sudo npm install
```

* Sur Windows :

```
#!sh

sudo npm install --no-bin-links
```

  
* Avant de commencer à travailler, dans la racine du projet, vous pouvez entrer : 

```
#!sh

gulp watch 
```


* Vous avez votre gulpfile.js à la racine qui contient des éléments relatifs à Elixir de Laravel. Voir la doc sur Laravel.com pour plus d'info.

* Si vous avez une erreur du type "Error in plugin 'gulp-notify'", vous pouvez entrer la ligne suivante dans votre Vagrant : 

```
#!sh

sudo apt-get install libnotify-bin
```